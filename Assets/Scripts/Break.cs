/*
 * пороигрывает звук при столкновении
 */
using UnityEngine;
using System.Collections;

public class Break : MonoBehaviour 
{
	public AudioClip Abreaking;
	AudioSource breaking;
	MainMenu other;
	void Start()
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		breaking=gameObject.AddComponent<AudioSource>();
		breaking.loop = false;
		breaking.playOnAwake = false;
		breaking.clip = Abreaking;
		breaking.panLevel = 0;
		if(other.toggleTxt)
		{
			breaking.volume = 0;
		}
		else 
		{
			breaking.volume = 0.7f;
		}
	}
	void OnCollisionEnter(Collision collision) 
	{
		if(collision.gameObject.name=="Car Ferrari 458 Italia N020511" || collision.gameObject.name=="Car Ferrari 458 Italia N020512" || collision.gameObject.name=="Car Ferrari 458 Italia N020513")
		{
			breaking.Play();
			rigidbody.isKinematic = false;
		}
    }
	
}
