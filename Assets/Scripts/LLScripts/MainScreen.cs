/*
 * скрипт ленивой загрузки главного меню 
 */
using UnityEngine;
using System.Collections;
using System.IO;

public class MainScreen : MonoBehaviour 
{
	Rect playGameRect;
	Rect optionsRect;
	Rect careerRect;
	Rect siteRect;
	Rect wingsRect;
	Rect howtoRect;
	
	public GUISkin optionSkin;
	public GUISkin careerSkin;
	public GUISkin practSkin;
	public GUISkin howtoSkin;
	public GUISkin siteSkin;
	public GUISkin wingsSkin;
	public GUISkin optionSkin2;
	public GUISkin careerSkin2;
	public GUISkin practSkin2;
	public GUISkin howtoSkin2;
	public GUISkin siteSkin2;
	public GUISkin wingsSkin2;
	
	int scaleGUI;
	MainMenu other;
	void Start () 
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		scaleGUI = Screen.width/1024;
		
		playGameRect  = new Rect(390*scaleGUI,245*scaleGUI,235*scaleGUI,80*scaleGUI);
		optionsRect   = new Rect(390*scaleGUI,445*scaleGUI,235*scaleGUI,80*scaleGUI);
		careerRect    = new Rect(390*scaleGUI,345*scaleGUI,235*scaleGUI,80*scaleGUI);
		howtoRect     = new Rect(810*scaleGUI, 700*scaleGUI, 167*scaleGUI, 53*scaleGUI);
		siteRect      = new Rect(458*scaleGUI, 700*scaleGUI, 95*scaleGUI, 37*scaleGUI);
		wingsRect     = new Rect(420*scaleGUI, 0*scaleGUI, 194*scaleGUI, 205*scaleGUI);
	}
	
	void OnGUI () 
	{
		GUI.skin=scaleGUI==2? practSkin : practSkin2;    
    	if(GUI.Button(playGameRect, ""))//Free
		{
			other.UnPause();
			other.gameMode=true;
			other.freeMode=true;
			other.i=1;
			other.timeSizeSave=true;
			if (File.Exists(other.tmpPath))
			{
				File.Delete(other.tmpPath);
			}
			other.secondCar = true;
			other.firstCar = false;
			other.thirdCar = false;
			if(other.isMiniMap) Application.LoadLevel("QCAR-FreeMini");
			else Application.LoadLevel("QCAR-Free");
			other.firstLoad=true;
			Destroy(gameObject);
    	}     
		GUI.skin= scaleGUI==2? careerSkin : careerSkin2;
		if(GUI.Button(careerRect, ""))//Career
		{
			other.careerMode=true;
			other.carChoiseMod=true;
			other.firstLoad=true;
			Destroy(gameObject);
    	}   
		GUI.skin= scaleGUI==2? optionSkin : optionSkin2;
    	if(GUI.Button(optionsRect,""))//Options
		{
    	    other.optionsMode = true;
			other.firstLoad=true;
			Destroy(gameObject);
    	}
		GUI.skin= scaleGUI==2? howtoSkin : howtoSkin2;
    	if(GUI.Button(howtoRect,""))//howto
		{
    	    other.howToPlayMode=true;
			other.firstLoad=true;
			Destroy(gameObject);
    	}
		GUI.skin= scaleGUI==2? siteSkin : siteSkin2;
    	if(GUI.Button(siteRect,""))//Site 
		{
    	    Application.OpenURL("http://www.ardriftracing.com");
    	}
		GUI.skin= scaleGUI==2? wingsSkin : wingsSkin2;
    	if(GUI.Button(wingsRect,""))//Wings
		{
    	    
    	}
	}
}
