/*
 * скрипт ленивой загрузки меню результатов 
 */
using UnityEngine;
using System.Collections;

public class Result : MonoBehaviour 
{
	public GUISkin successSkin;
	public GUISkin starActiveSkin;
	public GUISkin starNoActiveSkin;
	public GUISkin lineSkin;
	public GUISkin blackFoneSkin;
	public GUISkin siteSkin;
	public GUISkin wingsSkin;
	public GUISkin RestartSkin;
	public GUISkin nextMapSkin;
	public GUISkin resMenuSkin;
	
	public GUISkin successSkin2;
	public GUISkin starActiveSkin2;
	public GUISkin starNoActiveSkin2;
	public GUISkin lineSkin2;
	public GUISkin blackFoneSkin2;
	public GUISkin siteSkin2;
	public GUISkin wingsSkin2;
	public GUISkin RestartSkin2;
	public GUISkin nextMapSkin2;
	public GUISkin resMenuSkin2;
	
	Rect successRect;
	Rect firstStarRect;
	Rect secondStarRect;
	Rect thirdStarRect;
	Rect lineRect;
	Rect blackFoneRect;
	Rect siteRect;
	Rect wingsRect;
	Rect nextMapRect;
	Rect reMapRect;
	Rect menuRect;
	
	int scaleGUI;
	MainMenu other;
	void Start () 
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		scaleGUI = Screen.width/1024;
		lineRect       = new Rect(260*scaleGUI,550*scaleGUI,494*scaleGUI,12*scaleGUI);//Result
		blackFoneRect  = new Rect(260*scaleGUI, 200*scaleGUI, 494*scaleGUI, 356*scaleGUI);
		successRect    = new Rect(260*scaleGUI,245*scaleGUI,494*scaleGUI,37*scaleGUI);
		firstStarRect  = new Rect(440*scaleGUI,300*scaleGUI,45*scaleGUI,38*scaleGUI);
		secondStarRect = new Rect(490*scaleGUI,300*scaleGUI,45*scaleGUI,38*scaleGUI);
		thirdStarRect  = new Rect(540*scaleGUI,300*scaleGUI,45*scaleGUI,38*scaleGUI);
		siteRect      = new Rect(458*scaleGUI, 700*scaleGUI, 95*scaleGUI, 37*scaleGUI);
		wingsRect     = new Rect(420*scaleGUI, 0*scaleGUI, 194*scaleGUI, 205*scaleGUI);
		reMapRect      = new Rect(330*scaleGUI, 460*scaleGUI, 78*scaleGUI, 78*scaleGUI);
		nextMapRect    = new Rect(600*scaleGUI, 460*scaleGUI, 78*scaleGUI, 78*scaleGUI);
		menuRect       = new Rect(450*scaleGUI, 460*scaleGUI, 122*scaleGUI, 80*scaleGUI);
	}
	
	void OnGUI () 
	{
		GUI.skin= scaleGUI==2? blackFoneSkin : blackFoneSkin2;
		GUI.Label(blackFoneRect, "");//blackFone
		GUI.skin= scaleGUI==2? lineSkin : lineSkin2;
    	if(GUI.Button(lineRect,"")){}//line
		GUI.skin= scaleGUI==2? successSkin : successSkin2;
    	if(GUI.Button(successRect,"")){}//Success 
		
		if((other.scoreInGame/other.timeInGame<=0) || other.rammedCount>=other.maxBreakRammed)
		{
			GUI.skin= scaleGUI==2? starNoActiveSkin : starNoActiveSkin2;
			if(GUI.Button(firstStarRect,"")){}//Stars
    		if(GUI.Button(secondStarRect,"")){}
    		if(GUI.Button(thirdStarRect,"")){} 
		}
		else if(other.scoreInGame/other.timeInGame>0 && other.scoreInGame/other.timeInGame<4)
		{
			GUI.skin= scaleGUI==2? starActiveSkin : starActiveSkin2;
			if(GUI.Button(firstStarRect,"")){}//Stars
			GUI.skin= scaleGUI==2? starNoActiveSkin : starNoActiveSkin2;
    		if(GUI.Button(secondStarRect,"")){}
    		if(GUI.Button(thirdStarRect,"")){} 
		}
		else if(other.scoreInGame/other.timeInGame>=4 && other.scoreInGame/other.timeInGame<7)
		{
			GUI.skin= scaleGUI==2? starActiveSkin : starActiveSkin2;
			if(GUI.Button(firstStarRect,"")){}//Stars
    		if(GUI.Button(secondStarRect,"")){}
			GUI.skin= scaleGUI==2? starNoActiveSkin : starNoActiveSkin2;
    		if(GUI.Button(thirdStarRect,"")){} 
		}
		else if(other.scoreInGame/other.timeInGame>=7)
		{
			GUI.skin= scaleGUI==2? starActiveSkin : starActiveSkin2;
			if(GUI.Button(firstStarRect,"")){}//Stars
    		if(GUI.Button(secondStarRect,"")){}
    		if(GUI.Button(thirdStarRect,"")){} 
		}
    	
		GUI.skin= scaleGUI==2? siteSkin : siteSkin2;
    	if(GUI.Button(siteRect,""))//Site 
		{
    	    Application.OpenURL("http://www.ardriftracing.com");
    	}
		
		GUI.skin= scaleGUI==2? wingsSkin : wingsSkin2;
    	if(GUI.Button(wingsRect,"")){}//Wings
		
		
		GUI.skin= scaleGUI==2? RestartSkin : RestartSkin2;
		if(GUI.Button(reMapRect,""))     //Retry
		{
			other.isScoreLevel=false;
			other.firstLoad=true;
			ResultLoader.bundle.Unload(true);
			Application.LoadLevel(other.lastLoadLvl);
		}
		if(other.loadMapCount<5 && other.IsUnlockLvl(other.loadMapCount+2))//<5
		{
			GUI.skin= scaleGUI==2? nextMapSkin : nextMapSkin2;
			if(GUI.Button(nextMapRect,""))   //NextMap
			{
				other.loadMapCount+=2;
				other.isScoreLevel=false;
				other.firstLoad=true;
				ResultLoader.bundle.Unload(true);
				
				Application.LoadLevel(other.lastLoadLvl+2);
			}
		}
		/*if(other.loadMapCount>=5 && other.loadMapCount<49 && other.isPurchased)
		{
			GUI.skin= scaleGUI==2? nextMapSkin : nextMapSkin2;
			if(GUI.Button(nextMapRect,""))   //NextMap
			{
				other.isScoreLevel=false;
				other.firstLoad=true;
				ResultLoader.bundle.Unload(true);
				
				Application.LoadLevel(other.lastLoadLvl+2);
			}
		}*/
		GUI.skin= scaleGUI==2? resMenuSkin : resMenuSkin2;
		if(GUI.Button(menuRect,""))   //Menu
		{
			other.isScoreLevel=false;
			other.gameMode=false;
			other.freeMode=false;
			other.firstLoad=true;
			ResultLoader.bundle.Unload(true);
			other.Pause();
		}
	}
}
