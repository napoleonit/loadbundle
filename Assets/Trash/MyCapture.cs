using UnityEngine;
using System.Collections;

public class MyCapture : MonoBehaviour 
{

public float speed=0.5f;
    Vector3 position;
    bool go=false;
    float StartPos_z, StartPos_y, StartPos_x;
	Ray ray;
	void Start()
	{
		StartPos_x = transform.position.x;
        StartPos_y = transform.position.y;
        StartPos_z = transform.position.z;
	}
    void Update () 
    {

        if (Input.GetMouseButton(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity) && hit.collider.name == "Cube")
            {
                position = hit.point;
            }
            Vector3 direction = position - transform.position;
            float targ_pos = Vector3.Distance(transform.position, position);
            Vector3 temp = new Vector3(StartPos_x, StartPos_y, direction.z);
            transform.Translate(direction * speed, Space.World);
        }
    }
}