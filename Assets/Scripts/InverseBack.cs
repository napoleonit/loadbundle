using UnityEngine;
using System.Collections;

public class InverseBack : MonoBehaviour 
{
	MainMenu other;
	void Awake () 
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		if(other.inverse)
		{
			transform.position = new Vector3(0.0f,0,0);
		}
	}
	void Update()
	{
		if(other.inversionReload!=other.inverse)
		{
			other.inversionReload=other.inverse;
			other.optionsMode = false;
			other.optionsModeOnGame = false;
			other.firstLoad=true;
			OptionsLoader.bundle.Unload(true);
			Application.LoadLevel(Application.loadedLevelName);
		}
	}	
}
