using UnityEngine;
using System.Collections;

public class GUIScript : MonoBehaviour {
	public GUISkin infoSkin;
	public GUISkin colorsSkin;
	public GUISkin shareSkin;
	public GUISkin vkSkin;
	public GUISkin fbSkin;
	
	public GUISkin colorsSkin2;
	public GUISkin shareSkin2;
	public GUISkin vkSkin2;
	public GUISkin fbSkin2;
	
	Rect colorsRect;
	Rect shareRect;
	Rect vkRect;
	Rect fbRect;
	
	bool shareF = false;
	bool colorF =false;
	
	int scaleGUI;
	
	Rect Rect1;
	Rect Rect2;
	Rect Rect3;
	
	Rect infoRect;
	
	public Texture Tex1;
	public Texture Tex2;
	public Texture Tex3;
	
	public Texture info2;
	public Texture info3;
	
	public GameObject[] Mass;
	
	// двигаем камеру
    private bool drag = false;
    // масштабируем
    private bool zoom = false;

    // экранные координаты начальной точки касания
    private Vector3 initialTouchPosition;
    // мировые координаты камеры при инициировании
    // перемещения/масштабирования
    private Vector3 initialDivanPosition;

    // экранные координаты начальной точки первого касания
    private Vector3 initialTouch0Position;
    // экранные координаты начальной точки второго касания
    private Vector3 initialTouch1Position;
    // средняя точка между начальными координатами касаний
    private Vector3 initialMidPointScreen;
    // ортогональный размер камеры на момент начала масштабирования
    private Vector3 initialOrthographicSize;
	
	private Vector2 delta;
	private Vector2 oldAndNewPointDelta;
	
	// Use this for initialization
	void Start () {
		scaleGUI = Screen.width/1024;
		Rect1 = new Rect(0*scaleGUI, 575*scaleGUI, 91*scaleGUI, 91*scaleGUI);
		Rect2 = new Rect(0*scaleGUI, 480*scaleGUI, 91*scaleGUI, 91*scaleGUI);
		Rect3 = new Rect(0*scaleGUI, 385*scaleGUI, 91*scaleGUI, 91*scaleGUI);
		infoRect = new Rect(10*scaleGUI/2, 10*scaleGUI/2, 100*scaleGUI/2, 100*scaleGUI/2);
		
		colorsRect = new Rect(0*scaleGUI, 677*scaleGUI, 91*scaleGUI, 91*scaleGUI);
		shareRect = new Rect(933*scaleGUI, 677*scaleGUI, 91*scaleGUI, 91*scaleGUI);
		vkRect = new Rect(933*scaleGUI, 575*scaleGUI, 91*scaleGUI, 97*scaleGUI);
		fbRect = new Rect(933*scaleGUI, 480*scaleGUI, 91*scaleGUI, 97*scaleGUI);
	}
	
	void OnGUI(){
		/*GUI.skin = infoSkin;
		if(GUI.Button(infoRect, scaleGUI == 1? info2:info3)){Application.LoadLevel("InfoScene");}
		*/
		GUI.skin = scaleGUI==2? colorsSkin : colorsSkin2;
		if(GUI.Button(colorsRect, "")){colorF = !colorF;}
		if(colorF)
		{
			if(GUI.Button(Rect1, Tex1)){SetGOTexture(Mass, Tex1);}
			if(GUI.Button(Rect2, Tex2)){SetGOTexture(Mass, Tex2);}
			if(GUI.Button(Rect3, Tex3)){SetGOTexture(Mass, Tex3);}
		}
		
		/*GUI.skin = scaleGUI==2? shareSkin : shareSkin2;
		if(GUI.Button(shareRect, "")){shareF = !shareF;}
		if(shareF)
		{
			GUI.skin = scaleGUI==2? vkSkin : vkSkin2;
			if(GUI.Button(vkRect, "")){}
			
			GUI.skin = scaleGUI==2? fbSkin : fbSkin2;
			if(GUI.Button(fbRect, "")){}
		}*/
	}
	
	// Update is called once per frame
	void Update()
    {
        if (Input.touchCount == 1)
        {
            zoom = false;
            Touch touch0 = Input.GetTouch(0);

            if (IsTouching(touch0))
            {
				
                if (!drag)
                {
                    initialTouchPosition = touch0.position;
                    initialDivanPosition = GameObject.Find("prObject").transform.position;

                    drag = true;
                }
                else
                {
                    delta.x = touch0.position.x - initialTouchPosition.x;
					delta.y = touch0.position.y - initialTouchPosition.y;
                    Vector3 newPos = initialDivanPosition;
					newPos.x += delta.x;
                    newPos.y -= delta.y;
        			
                    
					
					//GameObject.Find("prObject").transform.position = newPos;
                    //this.transform.position = newPos;
                }
            }

            if (!IsTouching(touch0))
            {
                drag = false;
            }
        }
        else
        {
            drag = false;
        }

        if (Input.touchCount == 2)
        {
            drag = false;

            Touch touch0 = Input.GetTouch(0);
            Touch touch1 = Input.GetTouch(1);

            if (!zoom)
            {
                initialTouch0Position = touch0.position;
                initialTouch1Position = touch1.position;
                initialDivanPosition = GameObject.Find("prObject").transform.position;
                initialOrthographicSize = GameObject.Find("prObject").transform.localScale;
                initialMidPointScreen = (touch0.position + touch1.position) / 2;

                zoom = true;
            }
            else
            {
                //GameObject.Find("prObject").transform.position = initialDivanPosition;
                //GameObject.Find("prObject").transform.localScale = initialOrthographicSize;

                float scaleFactor = GetScaleFactor(touch0.position,
                                                   touch1.position,
                                                   initialTouch0Position,
                                                   initialTouch1Position);

                Vector2 currentMidPoint = (touch0.position + touch1.position) / 2;
                Vector3 initialPointWorldBeforeZoom = initialMidPointScreen;

                GameObject.Find("prObject").transform.localScale = initialOrthographicSize * scaleFactor;

                Vector3 initialPointWorldAfterZoom = initialMidPointScreen;
                Vector2 initialPointDelta = initialPointWorldBeforeZoom - initialPointWorldAfterZoom;

                oldAndNewPointDelta.x = currentMidPoint.x - initialMidPointScreen.x;
                oldAndNewPointDelta.y = currentMidPoint.y - initialMidPointScreen.y;

                Vector3 newPos = initialDivanPosition;
                newPos.x -= oldAndNewPointDelta.x - initialPointDelta.x;
                newPos.y -= oldAndNewPointDelta.y - initialPointDelta.y;

                //GameObject.Find("prObject").transform.position = newPos;
            }
        }
        else
        {
            zoom = false;
        }
    }
	
	static bool IsTouching(Touch touch)
    {
        return touch.phase == TouchPhase.Began ||
                touch.phase == TouchPhase.Moved ||
                touch.phase == TouchPhase.Stationary;
    }

    public static float GetScaleFactor(Vector2 position1, Vector2 position2, Vector2 oldPosition1, Vector2 oldPosition2)
    {
        float distance = Vector2.Distance(position1, position2);
        float oldDistance = Vector2.Distance(oldPosition1, oldPosition2);

        if (oldDistance == 0 || distance == 0)
        {
            return 1.0f;
        }

        return distance / oldDistance;
    }
	
	void SetGOTexture(GameObject[] GO, Texture tex)
	{
		foreach(GameObject tmp in GO)
		{
			tmp.renderer.material.mainTexture = tex;
		}
	}
}
