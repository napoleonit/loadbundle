/*
 *срабатывает при финише...считает сколько сбил, открывает следующий уровень 
 */
using UnityEngine;
using System.Collections;
using System.IO;

public class FinishSecondScene : MonoBehaviour 
{
	public bool[] dot = {false, false, false};
	public bool go;
	public bool isSprint;
	public bool isCarParking;
	public bool dotCarParking;
	public bool finCarParking;
	float time;
	int lapCount;
	MainMenu menu;
	public GUISkin textStyle;
	public GUISkin textStyle2;
	
	public string nextLvl;
	public int rammedCount;
	public int maxBreakRammed;
	void Start () 
	{
		nextLvl=GetPath()+nextLvl;
		rammedCount=-1;
		lapCount=0;
		menu = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		go=false;
		time=0;
	}
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.name=="chassis22" && dot[0] && dot[1] && dot[2])
		{
			for(int i=0; i<3; i++) dot[i]=false;
			if(isSprint) 
			{
				go=true;
				OpenNextLvl();
			}
			else
			{
				for(int i=0; i<3; i++) dot[i]=false;
				lapCount++;
				if(lapCount==3) 
				{
					lapCount=0;go=true;
					OpenNextLvl();
				}
			}
		}
		dot[0]=false;
    }
	void OnGUI()
	{
		GUI.skin = menu.scaleGUI==2? textStyle : textStyle2;
		if(isSprint) GUI.Button (new Rect (900*menu.scaleGUI,130*menu.scaleGUI,100*menu.scaleGUI*2,30*menu.scaleGUI*2), "Laps: "+(lapCount+1)+"/"+"1");
		else 		 GUI.Button (new Rect (900*menu.scaleGUI,130*menu.scaleGUI,100*menu.scaleGUI*2,30*menu.scaleGUI*2), "Laps: "+(lapCount+1)+"/"+"3");
	}
	void Update()
	{
		menu.rammedCount=rammedCount;
		menu.maxBreakRammed=maxBreakRammed;
		if(go)
		{
			time+=Time.deltaTime;
			if(time>0.5f)
			{
				go=false;
				time=0;
				menu.isScoreLevel=true;
				Application.LoadLevel("QCAR-Score");
			}
		}
	}
	string GetPath()
	{
		string root = Application.dataPath.Substring(0, Application.dataPath.Length - 13) + "/Documents";
		#if UNITY_EDITOR
		root = Application.dataPath.Substring(0, Application.dataPath.Length);
		#endif
        return root;
	}
	
	void OpenNextLvl()
	{
		menu.lastLoadLvl = Application.loadedLevel;
		if(rammedCount<maxBreakRammed)
		{
			if (!File.Exists(nextLvl))
			{
       			using (StreamWriter sw = File.CreateText(nextLvl))
       			{
       			    sw.WriteLine("0");
					sw.WriteLine("0.0");
					sw.Close();
       			}
			}
		}
	}
}
