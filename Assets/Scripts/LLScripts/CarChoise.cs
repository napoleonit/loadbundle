/*
 * скрипт ленивой загрузки, подгружает меню выбора машин
 */
using UnityEngine;
using System.Collections;

public class CarChoise : MonoBehaviour 
{
	Rect wingsMiniRect;
	Rect nextTrackRect;
	Rect lastTrackRect;
	Rect firstCarStatRect;
	Rect secondCarStatRect;
	
	public GUISkin wingsMiniSkin;
	public GUISkin nextTrackSkin;
	public GUISkin lastTrackSkin;
	public GUISkin toogleCtrlSkin;
	public GUISkin firstCarStatStyle;
	public GUISkin secondCarStatStyle;
	
	public GUISkin wingsMiniSkin2;
	public GUISkin nextTrackSkin2;
	public GUISkin lastTrackSkin2;
	public GUISkin toogleCtrlSkin2;
	public GUISkin firstCarStatStyle2;
	public GUISkin secondCarStatStyle2;
	
	public Material firstCarMaterial;
	public Material secondCarMaterial;
	
	int scaleGUI;
	MainMenu other;
	void Start()
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		scaleGUI = Screen.width/1024;
		nextTrackRect = new Rect(975*scaleGUI,345*scaleGUI,69*scaleGUI,175*scaleGUI);
		lastTrackRect = new Rect(-20*scaleGUI,345*scaleGUI,69*scaleGUI,175*scaleGUI);
		wingsMiniRect = new Rect(480*scaleGUI, 50*scaleGUI, 61.5f*scaleGUI, 69.5f*scaleGUI);
		firstCarStatRect   = new Rect(390*scaleGUI,600*scaleGUI,261*scaleGUI,70*scaleGUI);
		secondCarStatRect  = new Rect(390*scaleGUI,600*scaleGUI,261*scaleGUI,70*scaleGUI);
		other.nextBackButton=true;
	}
	void OnGUI () 
	{
		GUI.skin= scaleGUI==2? wingsMiniSkin : wingsMiniSkin2;
    	if(GUI.Button(wingsMiniRect,""))//WingsMini
		{
    	}
		/*GUI.skin= scaleGUI==2? nextTrackSkin : nextTrackSkin2;
		if(GUI.Button(nextTrackRect,""))//next car
		{
			if(other.carCount==3) other.carCount=1;
			other.carCount++;
    	}
		GUI.skin= scaleGUI==2? lastTrackSkin : lastTrackSkin2;
		if(GUI.Button(lastTrackRect,""))//last car
		{
			if(other.carCount==2) other.carCount=4;
			other.carCount--;
    	}		*/
		/*if(other.thirdCar)
		{
			//RedCarMain();
			GameObject.Find("Plane").renderer.material = firstCarMaterial;
			GUI.skin= scaleGUI==2? firstCarStatStyle : firstCarStatStyle2;
			if(GUI.Button(firstCarStatRect,"")){}
		}*/
		if(other.secondCar)
		{
			//GreenCarMain();
			GameObject.Find("Plane").renderer.material = secondCarMaterial;
			GUI.skin= scaleGUI==2? secondCarStatStyle : secondCarStatStyle2;
			if(GUI.Button(secondCarStatRect,"")){}
		}
	}
}
