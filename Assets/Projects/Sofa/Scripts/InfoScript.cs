using UnityEngine;
using System.Collections;
//using System.Net.Mail;
using System;
using System.Text.RegularExpressions;
using System.Text;

public class InfoScript : MonoBehaviour {
	
	string screenShotURL= "http://itnap.ru/forms/for_ar/tes/form.php";
	string res = "";
	public TextAsset marker;
	public string email = "";
	public Texture InfoFon;
		
	int scaleGUI;
	
	public GUISkin ButtonSkin;
	public GUISkin EditSkin;
	public GUISkin LabelSkin;
	
	public Texture back;
	public Texture send;
	
	Rect emailRect;
	Rect backRect;
	Rect sendRect;
	Rect resRect;
	
	bool show = false;
	// Use this for initialization
	void Start () {
		scaleGUI = Screen.width/1024;
		
		emailRect = new Rect(185*scaleGUI/2, 630*scaleGUI/2, 500*scaleGUI/2, 80*scaleGUI/2);
		
		backRect = new Rect(824*scaleGUI/2, 1350*scaleGUI/2, 400*scaleGUI/2, 100*scaleGUI/2);
		
		sendRect = new Rect(675*scaleGUI/2, 620*scaleGUI/2, 100*scaleGUI/2, 100*scaleGUI/2);
		
		resRect = new Rect(185*scaleGUI/2, 720*scaleGUI/2, 1000*scaleGUI/2, 100*scaleGUI/2);
	}
	
	void OnGUI() {
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), InfoFon);
		
		GUI.skin = ButtonSkin;
		if(GUI.Button(backRect, back)){Application.LoadLevel("Vuforia-4-ImageTargets");}
		if(GUI.Button(sendRect, send)){StartCoroutine(SendMail());}
		
		GUI.skin = EditSkin;
		email = GUI.TextField (emailRect, email);
		
		if(scaleGUI == 1){
			GUI.skin.label.fontSize = 20;
			GUI.skin.textField.fontSize = 20;
		}
		else{
			GUI.skin.label.fontSize = 40;
			GUI.skin.textField.fontSize = 40;
		}
		
		if(show){
			GUI.Label(resRect, res);
		}
		//if(GUI.Button(new Rect(10, 10, 100, 100), "e-mail")){StartCoroutine(SendMail());}
		//if(GUI.Button(new Rect(250, 10, 100, 100), "Back")){Application.LoadLevel("Vuforia-4-ImageTargets");}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	IEnumerator SendMail()
	{
    	/*var width = Screen.width;
    	var height = Screen.height;
    	var tex = new Texture2D( width, height, TextureFormat.RGB24, false );
    	tex.ReadPixels(new Rect(0, 0, width, height), 0, 0 );
    	tex.Apply();
		
    	var bytes = tex.EncodeToPNG();
    	Destroy( tex );*/
		
		email = GUI.TextField (emailRect, email);
		//if(IsValid(email)){
			var bytes = marker.bytes;
    		var form = new WWWForm();
    		//form.AddField("email", "ukleonov@mail.ru");
			form.AddField("email", email);
    		form.AddBinaryData("img", bytes, "marker.pdf", "");
    		WWW w = new WWW(screenShotURL, form);
    		yield return w;
    		if (!string.IsNullOrEmpty(w.error)){
    	    	print("Error response"+w.error);
				res = w.error;
			}
    		else{
    	    	print("Response: "+w.text);
				res = w.text;
			}
		/*}
		else{
			res = "Неправильно введен адрес!";
		}*/
		show = true;
	}
	
	/*public bool IsValid(string emailaddress)
	{
		Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
		Match match = regex.Match(emailaddress);
		if (match.Success)
    		return true;
		else
    		return false;
	}*/
}
