var distance:float;

function Start()
{
	distance=0; 
} 

function LateUpdate () 
{      
        if (Input.touchCount == 2)
        {
            var touch0 = Input.GetTouch(0);
            var touch1 = Input.GetTouch(1);
 
            var curDist = touch0.position - touch1.position;
            var prevDist = (touch0.position - touch0.deltaPosition) - (touch1.position - touch1.deltaPosition);
 
            var delta = (curDist.magnitude - prevDist.magnitude)/50;
            distance -= delta;
 
            if(distance>10.0)
                distance=10.0;
            if(distance<1)
            	distance=1;    
        }
 
    	//Update View
        //var rotation = Quaternion.Euler(y, x, 0);
        //var position = rotation * Vector3(0.0, 0.0, -distance) + target.position;
 
        //transform.rotation = rotation;
        //transform.position = position;  
        transform.localScale = Vector3(distance, distance, distance);  
} 