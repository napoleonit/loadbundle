using UnityEngine;
using System.Collections;

public class gui : MonoBehaviour 
{
	public GUISkin colorPanelSkin;
	public GUISkin colorWallSkin;
	public GUISkin colorKrishaSkin;
	public GUISkin choiseKrishaSkin;
	public GUISkin choiseWallSkin;
	public GUISkin choisePanelSkin;
	public GUISkin colorFoneSkin;
	
	public GUISkin colorPanelSkin2;
	public GUISkin colorWallSkin2;
	public GUISkin colorKrishaSkin2;
	public GUISkin choiseKrishaSkin2;
	public GUISkin choiseWallSkin2;
	public GUISkin choisePanelSkin2;
	public GUISkin colorFoneSkin2;
	
	public GUISkin infoSkin;
	
	Rect colorPanelRect;
	Rect colorWallRect;
	Rect colorKrishaRect;
	Rect choiseWallRect;
	Rect choiseKrishaRect;
	Rect choisePanelRect;
	Rect colorFoneRect;
	Rect colorFoneRect2;
	Rect colorFoneRect3;
	
	Rect greenRect;
	Rect grayRect;
	Rect orangeRect;
	
	Rect greenRect2;
	Rect grayRect2;
	Rect orangeRect2;
	
	Rect greenRect3;
	Rect grayRect3;
	Rect orangeRect3;
	
	Rect infoRect;
	
	public Texture ButtonRoof1;
	public Texture ButtonRoof2;
	public Texture ButtonRoof3;
	
	public Texture ButtonWall1;
	public Texture ButtonWall2;
	public Texture ButtonWall3;
	
	public Texture ButtonPanel1;
	public Texture ButtonPanel2;
	public Texture ButtonPanel3;
	
	public Texture grayTex;
	public Texture greenTex;
	public Texture orangeTex;
	
	public Texture grayTex2;
	public Texture greenTex2;
	public Texture orangeTex2;
	
	public Texture info2;
	public Texture info3;
	
	public GameObject[] RoofMass;
	public GameObject[] WallMass;
	public GameObject[] PanelMass;
	
	public Texture RoofTex1;
	public Texture RoofTex2;
	public Texture RoofTex3;
	
	public Texture WallTex1;
	public Texture WallTex2;
	public Texture WallTex3;
	
	public Texture PanelTex1;
	public Texture PanelTex2;
	public Texture PanelTex3;
	
	bool colorF = false;
	bool texWall = false;
	bool texPanel = false;
	
	int Roof = 0;
	int Wall = 0;
	int Panel = 0;
	
	int scaleGUI;
	
	// Use this for initialization
	void Start () 
	{
		scaleGUI = Screen.width/1024;
		
		colorPanelRect = new Rect(1866*scaleGUI/2, 1354*scaleGUI/2, 182*scaleGUI/2, 182*scaleGUI/2);
		colorWallRect= new Rect(1866*scaleGUI/2, 1168*scaleGUI/2, 182*scaleGUI/2, 182*scaleGUI/2);
		colorKrishaRect= new Rect(1866*scaleGUI/2, 982*scaleGUI/2, 182*scaleGUI/2, 182*scaleGUI/2);
		choiseWallRect= new Rect(0, 1168*scaleGUI/2, 182*scaleGUI/2, 182*scaleGUI/2);
		choiseKrishaRect= new Rect(0, 982*scaleGUI/2, 182*scaleGUI/2, 182*scaleGUI/2);
		choisePanelRect= new Rect(0, 1354*scaleGUI/2, 182*scaleGUI/2, 182*scaleGUI/2);
		colorFoneRect = new Rect(1335*scaleGUI/2, 982*scaleGUI/2, 522*scaleGUI/2, 183*scaleGUI/2);
		colorFoneRect3 = new Rect(1335*scaleGUI/2, 1168*scaleGUI/2, 522*scaleGUI/2, 183*scaleGUI/2);
		colorFoneRect2 = new Rect(1335*scaleGUI/2, 1354*scaleGUI/2, 522*scaleGUI/2, 183*scaleGUI/2);
		
		grayRect = new Rect(1358*scaleGUI/2, 997*scaleGUI/2, 155*scaleGUI/2, 155*scaleGUI/2);
		greenRect = new Rect(1514*scaleGUI/2, 997*scaleGUI/2, 155*scaleGUI/2, 155*scaleGUI/2);
		orangeRect = new Rect(1670*scaleGUI/2, 997*scaleGUI/2, 155*scaleGUI/2, 155*scaleGUI/2);
		
		grayRect3 = new Rect(1358*scaleGUI/2, 1183*scaleGUI/2, 155*scaleGUI/2, 155*scaleGUI/2);
		greenRect3 = new Rect(1514*scaleGUI/2, 1183*scaleGUI/2, 155*scaleGUI/2, 155*scaleGUI/2);
		orangeRect3 = new Rect(1670*scaleGUI/2, 1183*scaleGUI/2, 155*scaleGUI/2, 155*scaleGUI/2);
		
		grayRect2 = new Rect(1358*scaleGUI/2, 1369*scaleGUI/2, 155*scaleGUI/2, 155*scaleGUI/2);
		greenRect2 = new Rect(1514*scaleGUI/2, 1369*scaleGUI/2, 155*scaleGUI/2, 155*scaleGUI/2);
		orangeRect2 = new Rect(1670*scaleGUI/2, 1369*scaleGUI/2, 155*scaleGUI/2, 155*scaleGUI/2);
		
		infoRect = new Rect(10*scaleGUI/2, 10*scaleGUI/2, 100*scaleGUI/2, 100*scaleGUI/2);
	}
	
	void OnGUI()
	{
		/*GUI.skin = infoSkin;
		if(scaleGUI == 1){
			if(GUI.Button(infoRect, info2)){Application.LoadLevel("InfoScene");}
		}
		else{
			if(GUI.Button(infoRect, info3)){Application.LoadLevel("InfoScene");}
		}
		
		if(GUI.Button(infoRect, "Info")){Application.LoadLevel("InfoScene");}*/
		
		GUI.skin = scaleGUI==2? colorKrishaSkin : colorKrishaSkin2;
		if(GUI.Button(colorKrishaRect, "")){colorF = !colorF;}
		
		if(colorF)
		{
			GUI.skin = scaleGUI==2? colorFoneSkin : colorFoneSkin2;
			GUI.Label(colorFoneRect, "");
			GUI.skin = scaleGUI==2? colorKrishaSkin : colorKrishaSkin2;
			if(GUI.Button(grayRect, ButtonRoof1)){SetGOTexture(RoofMass, RoofTex1);}
			if(GUI.Button(greenRect, ButtonRoof2)){SetGOTexture(RoofMass, RoofTex2);}
			if(GUI.Button(orangeRect, ButtonRoof3)){SetGOTexture(RoofMass, RoofTex3);}
		}
		
		GUI.skin = scaleGUI==2? colorPanelSkin : colorPanelSkin2;
		if(GUI.Button(colorPanelRect, "")){texPanel = !texPanel;}
		
		if(texPanel)
		{
			GUI.skin = scaleGUI==2? colorFoneSkin : colorFoneSkin2;
			GUI.Label(colorFoneRect2, "");
			GUI.skin = scaleGUI==2? colorPanelSkin : colorPanelSkin2;
			if(GUI.Button(grayRect2, ButtonPanel1)){SetGOTexture(PanelMass, PanelTex1);}
			if(GUI.Button(greenRect2, ButtonPanel2)){SetGOTexture(PanelMass, PanelTex2);}
			if(GUI.Button(orangeRect2, ButtonPanel3)){SetGOTexture(PanelMass, PanelTex3);}
		}
		
		GUI.skin = scaleGUI==2? colorWallSkin : colorWallSkin2;
		if(GUI.Button(colorWallRect, "")){texWall = !texWall;}
		
		if(texWall)
		{
			GUI.skin = scaleGUI==2? colorFoneSkin : colorFoneSkin2;
			GUI.Label(colorFoneRect3, "");
			GUI.skin = scaleGUI==2? colorWallSkin : colorWallSkin2;
			if(GUI.Button(grayRect3, ButtonWall1)){SetGOTexture(WallMass, WallTex1);}
			if(GUI.Button(greenRect3, ButtonWall2)){SetGOTexture(WallMass, WallTex2);}
			if(GUI.Button(orangeRect3, ButtonWall3)){SetGOTexture(WallMass, WallTex3);}
		}
		
		GUI.skin = scaleGUI==2? choiseWallSkin : choiseWallSkin2;
		if(GUI.Button(choiseWallRect, "")){WallSwitcher();}
		GUI.skin = scaleGUI==2? choiseKrishaSkin : choiseKrishaSkin2;
		if(GUI.Button(choiseKrishaRect, "")){RoofSwitcher();}
		GUI.skin = scaleGUI==2? choisePanelSkin : choisePanelSkin2;
		if(GUI.Button(choisePanelRect, "")){PanelSwitcher();}
	}
	
	void RoofSwitcher()
    {
			switch(Roof)
			{
				case(0):{
				GameObject.Find("krisha s oknami").transform.localScale = new Vector3(0,0,0);
				Roof = 1;
				break;
				}
				case(1):{
				GameObject.Find("krisha s oknami").transform.localScale = new Vector3(1,1,1);
				Roof = 0;
				break;
				}
			}
    }
	
	void WallSwitcher()
    {
			switch(Wall)
			{
				case(0):{
				GameObject.Find("Wall").transform.localScale = new Vector3(0,0,0);
				Wall = 1;
				break;
				}
				case(1):{
				GameObject.Find("Wall").transform.localScale = new Vector3(1,1,1);
				Wall = 0;
				break;
				}
			}
    }
	
	void PanelSwitcher()
    {
		switch(Panel)
		{
			case(0):{
			GameObject.Find("1 etazh").transform.localScale = new Vector3(0,0,0);
			Panel = 1;
			break;
			}
			case(1):{
			GameObject.Find("1 etazh").transform.localScale = new Vector3(1,1,1);
			Panel = 0;
			break;
			}
		}
    }
	
	void SetGOTexture(GameObject[] GO, Texture tex)
	{
		foreach(GameObject tmp in GO)
		{
			tmp.renderer.material.mainTexture = tex;
		}
	}
}
