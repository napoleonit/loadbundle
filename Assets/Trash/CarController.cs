/*
 * отвечает за двежение машины, поворот колес и тд
 */ 
using UnityEngine;
using System.Collections;
using System;

public class CarController : MonoBehaviour
{
	public Joystick moveTouchPad;
    public Joystick rotateTouchPad;
	public Joystick backTouchPad;
	public Joystick stopTouchPad;

	float axisH=0;
	float axisV=0;

    public WheelCollider[] WColForward;
    public WheelCollider[] WColBack;

    public Transform[] wheelsF; //1
    public Transform[] wheelsB; //1

    public float wheelOffset = 0.1f; //2
    public float wheelRadius = 0.4f; //2

    public float maxSteer = 35;
    public float maxAccel = 35;
	public float maxForwardAccel = 35;
    public float maxBrake = 75;

    public Transform COM;
	
	//public AudioClip Astay;
	
	//AudioSource stay;
	
	private bool forwardJoy;
	float lastWhellCoord;
	int whellCount;
	
	MainMenu other;

	private Transform thisTransform;
	private CharacterController character;
	
    public class WheelData
    { //3
        public Transform wheelTransform; //4
        public WheelCollider col; //5
        public Vector3 wheelStartPos; //6 
        public float rotation = 0.0f;  //7
    }

    protected WheelData[] wheels; //8
		
    void Start()
    {	
		whellCount=1;
		lastWhellCoord=0;
		forwardJoy=false;
		
		//other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		
		//SetHardComplexity();
		SetEasyComplexity();
		
		//stay=gameObject.AddComponent<AudioSource>();
		//stay.loop = true;
		//stay.playOnAwake = true;
		//stay.clip = Astay;
		//stay.panLevel = 0;
		//stay.Play();
		
		thisTransform = GetComponent<Transform>();
		character = GetComponent<CharacterController>();
		
        rigidbody.centerOfMass = COM.localPosition;

        wheels = new WheelData[WColForward.Length + WColBack.Length]; //8

        for (int i = 0; i < WColForward.Length; i++)
        { //9
            wheels[i] = SetupWheels(wheelsF[i], WColForward[i]); //9
        }

        for (int i = 0; i < WColBack.Length; i++)
        { //9
            wheels[i + WColForward.Length] = SetupWheels(wheelsB[i], WColBack[i]); //9
        }
    }
	
	void Update()
	{

		//stay.volume = 1;
		axisV=axisH=0;
		Vector3 movement = thisTransform.TransformDirection(new Vector3( moveTouchPad.position.x, 0, moveTouchPad.position.y ) );

		movement.y = 0;
		movement.Normalize();
		while(true)
		{
			if(backTouchPad.position.y > 0.01)//back
			{
				axisV=-1;
				//if(forwardJoy) stay.pitch=1f;
				//if(stay.pitch<1.8f) stay.pitch+=(0.006f*backTouchPad.position.y);
				forwardJoy=false;
				break;
			}
			if ( moveTouchPad.position.y > 0.01 ) //forward
			{
				axisV=1;
				//if(!forwardJoy) stay.pitch=1f;
				//if(stay.pitch<1.8f) stay.pitch+=(0.006f*moveTouchPad.position.y);
				forwardJoy=true;
				break;
			}
			if(moveTouchPad.position.y == 0)
			{
				//if(stay.pitch>1f) stay.pitch-=0.04f;
				break;
			}
		}
		if(rotateTouchPad.position.x > 0)
		{
			axisH=rotateTouchPad.position.x;
			lastWhellCoord=axisH;
			//GameObject.Find("rule").transform.eulerAngles = new Vector3(0,90*rotateTouchPad.position.x+180,0);
			whellCount=1;
		}
		else if(rotateTouchPad.position.x < 0) 
		{
			axisH=rotateTouchPad.position.x;
			lastWhellCoord=axisH;
			//GameObject.Find("rule").transform.eulerAngles = new Vector3(0,90*rotateTouchPad.position.x+180,0);
			whellCount=1;
		}		
		else if(rotateTouchPad.position.x == 0) //ненужная часть, отвечающая за визуальный поворот руля
		{
			if((90*Math.Abs(lastWhellCoord)-whellCount)+180>180)
			{
				if(lastWhellCoord>0)
				{
					//GameObject.Find("rule").transform.eulerAngles = new Vector3(0,(90*lastWhellCoord-whellCount)+180,0);
				}
				else
				{
					//GameObject.Find("rule").transform.eulerAngles = new Vector3(0,(90*lastWhellCoord+whellCount)+180,0);
				}
				whellCount+=20;
			}
			else
			{
				//GameObject.Find("rule").transform.eulerAngles = new Vector3(0,180,0);
			}
		}	
#if UNITY_EDITOR	
		axisV = Input.GetAxis("Vertical"); 
       	axisH = Input.GetAxis("Horizontal");
#endif
		CarMove(axisV, axisH, (stopTouchPad.position.y>0.01));
        UpdateWheels();
	}
	
	float InputGetAxis(string axis)
	{
		float v=Input.GetAxis (axis);
		if(Mathf.Abs(v)>0.005) return v;
		if(axis=="Horizontal") return axisH;
		if(axis=="Vertical") return axisV;
		return 0;
	}

    private WheelData SetupWheels(Transform wheel, WheelCollider col)
    { //10
        WheelData result = new WheelData();

        result.wheelTransform = wheel; //10
        result.col = col; //10
        result.wheelStartPos = wheel.transform.localPosition; //10

        return result; //10

    }

    void FixedUpdate()
    {

    /*    float accel = 0;
        float steer = 0;

        accel = Input.GetAxis("Vertical");
        steer = Input.GetAxis("Horizontal");

        CarMove(accel, steer);
        UpdateWheels(); //11*/
    }


    private void UpdateWheels()//крутит и поворачивает колеса
    { //11
        float delta = Time.fixedDeltaTime; //12


        foreach (WheelData w in wheels)
        { //13
            WheelHit hit; //14

            Vector3 lp = w.wheelTransform.localPosition; //15
            if (w.col.GetGroundHit(out hit))
            { //16
                lp.y -= Vector3.Dot(w.wheelTransform.position - hit.point, transform.up) - wheelRadius; //17
            }
            else
            { //18

                lp.y = w.wheelStartPos.y - wheelOffset; //18
            }
            w.wheelTransform.localPosition = lp; //19


            w.rotation = Mathf.Repeat(w.rotation + delta * w.col.rpm * 360.0f / 60.0f, 360f); //20
            w.wheelTransform.localRotation = Quaternion.Euler(w.rotation, w.col.steerAngle, 0.0f); //21
        }

    }

    private void CarMove(float accel, float steer, bool handBrake)//двигает машину
    {

        foreach (WheelCollider col in WColForward)
        {
            col.steerAngle = steer * maxSteer;
        }

        if (accel == 0)
        {
            foreach (WheelCollider col in WColBack)
            {
                col.brakeTorque = maxBrake;
				col.motorTorque = 0;
            }
			foreach (WheelCollider col in WColForward)//передние колеса тоже привод ()для полного привода
            {
                col.brakeTorque = maxBrake;
				col.motorTorque = 0;
            }
        }
        else
        {
            foreach (WheelCollider col in WColBack)
            {
                col.brakeTorque = 0;
                col.motorTorque = accel * maxAccel;
            }
			foreach (WheelCollider col in WColForward)//для полного привода и передние колеса
            {
                col.brakeTorque = 0;
                col.motorTorque = accel * maxForwardAccel;
            }
        }
		if(handBrake)//ручник для харда
		{
			foreach (WheelCollider col in WColBack)
            {
                col.brakeTorque = maxBrake*3;
                col.motorTorque = 0;
            }	
		}
    }
	private void SetEasyComplexity()// установка легкой физики
	{
		maxSteer = 35;    //50   //поворот колес
		maxAccel = 44;  //40     // ускорение
		maxForwardAccel = 66;  //60    // разгон
		maxBrake = 30;   //55     //тормоз
	}
	private void SetHardComplexity()//установка сложной физики
	{
		maxSteer = 35;
		maxAccel = 35;
		maxForwardAccel = 35;
		maxBrake = 30;
	}
}
