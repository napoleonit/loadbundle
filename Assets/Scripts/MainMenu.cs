/*
 * Главный скрипт, отвечает за все переходы в меню
 */

using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Runtime.InteropServices;

public class MainMenu : MonoBehaviour 
{
	public GUISkin optionSkin;     //дизайн 3 ipad
	public GUISkin customSkin;
	public GUISkin pauseSkin;
	public GUISkin backSkin;
	public GUISkin coneSkin;
	public GUISkin RestartSkin;
	public GUISkin textStyle;
	public GUISkin menuOnGameStyle;
	public GUISkin backOnGameStyle;
	public GUISkin fonOnGameStyle;
	public GUISkin firstMapSkin;
	public GUISkin buySkin;
	public GUISkin restorePurSkin;
	public GUISkin saveMarkerSkin;
	public GUISkin nextSkin;
	
	public GUISkin pauseSkin2;     //дизайн 2 ipad
	public GUISkin backSkin2;
	public GUISkin optionSkin2;
	public GUISkin RestartSkin2;
	public GUISkin firstMapSkin2;
	public GUISkin textStyle2;
	public GUISkin menuOnGameStyle2;
	public GUISkin backOnGameStyle2;
	public GUISkin fonOnGameStyle2;
	public GUISkin buySkin2;
	public GUISkin PlayGameSkin2;
	public GUISkin restorePurSkin2;
	public GUISkin saveMarkerSkin2;
	public GUISkin nextSkin2;
	
	Rect optionsRect;
	Rect pauseRect;
	Rect quitRect;
	Rect RestartRect;
	Rect firstMap;
	Rect restorePurRect;
	Rect buyRect;
	Rect menuOnGameRect;
	Rect backOnGameRect;
	Rect fonOnGameRect;
	Rect firstMapRect;
	Rect saveMarkerRect;
	
	public int scaleGUI;
	public int i=0;
	float coneTime;
	float startPos;
	bool go;
	
	public bool toggleTxt=false;
	public bool inverse=false;
	public bool pause;
	public bool gameMode = false; 
	public bool freeMode=false;
	public bool isScoreLevel=false;
	public bool pauseOnGameMode=false;
	public  bool howToPlayMode=false;
	public bool optionsMode = false; 
	public bool careerMode = false;
	public bool buildMap=false;
	public bool coneFin=false;
	public bool timeSizeSave=false;
	public bool isMiniMap=false;
	public bool trackChoiseMod=false;
	public bool firstCar=false;
	public bool secondCar=true;
	public bool thirdCar=false;
	public bool carChoiseMod=false;
	public bool optionsModeOnGame = false; 
	
	string path;
	public string tmpPath;
	string clearPath;
	string firstLvl;
	string secondLvl;
	string thirdLvl;
	string fourthLvl;
	string fifthLvl;
	string sixthLvl;
	string seventhLvl;
	string eighthLvl;
	string nineLvl;
	string tenLvl;
	string elevenLvl;
	string twelveLvl;
	string thirteenLvl;
	string fourteenLvl;
	string fifteenLvl;
	string sixteenLvl;
 	string seventeenLvl;
	string eighteenLvl;
	string nineteenLvl; 
	string twentyLvl; 
	string twentyoneLvl;
	string twentytwoLvl;
	string twentythreeLvl;
	string twentyfourLvl;
	string twentyfiveLvl;
	
	StreamWriter sw;
	
	float x1=0;
	float y1=0;
	float z1=0;
	int roadCount=1;
	
	Vector3 tapLast;
	
	public float speed;
	string strSpeed;

	public int carCount;
	public GameObject Car;
	
	public float comfortZone = 70.0f;      //Swipe section
    public float minSwipeDist = 14.0f;
    public float maxSwipeTime = 0.5f;
    private float startTime;
    private Vector2 swipeStartPos;
    private bool couldBeSwipe;	
	public bool mySwipe=false;
    public enum SwipeDirection {None, Up, Down}
    public SwipeDirection lastSwipe = MainMenu.SwipeDirection.None;
    public float lastSwipeTime;
	
	public int lastLoadLvl;
	public int loadMapCount;
	
	public bool isPurchased = false;
	public bool isHard;
	
	public int starCount;
	public float timeInGame=1.0f;
	public int scoreInGame=-1;
	
	public GameObject mainScreenGO;
	public GameObject loaderGO;
	public GameObject loaderHTP;
	public GameObject loaderOPT;
	public GameObject loaderCH;
	public GameObject loaderRES;
	public bool firstLoad;
	
	public int rammedCount=0;
	public int maxBreakRammed=0;
	
	public Material mainMaterial;
	
	public bool nextBackButton;
	public bool inversionReload;
	float timeReverse;
	void Awake()
	{
    	DontDestroyOnLoad(this);
	}
	void Start()
	{
		timeReverse = 0.0f;
		inversionReload=false;
		nextBackButton=false;
		firstLoad=true;
		isHard = false;
		loadMapCount=1;
		scaleGUI =Screen.width/1024;
		SetGUIScale();
		carCount = 3;
		coneTime=0;
		path = GetPath() + "/Position.txt";
		tmpPath=GetPath() + "/tmp.txt";
		clearPath=GetPath() + "/ClearMap.txt";
		firstLvl = GetPath() + "/firstLvl";
		secondLvl = GetPath() + "/secondLvl";
		thirdLvl = GetPath() + "/thirdLvl";
		fourthLvl = GetPath() + "/fourthLvl";
		fifthLvl = GetPath() + "/fifthLvl";
		sixthLvl = GetPath() + "/sixthLvl";
		seventhLvl = GetPath() + "/seventhLvl";
		eighthLvl = GetPath() + "/eighthLvl";
		nineLvl = GetPath() + "/nineLvl";
		tenLvl = GetPath() + "/tenLvl";
		elevenLvl = GetPath() + "/elevenLvl";
		twelveLvl = GetPath() + "/twelveLvl";
		thirteenLvl = GetPath() + "/thirteenLvl";
		fourteenLvl = GetPath() + "/fourteenLvl";
		fifteenLvl = GetPath() + "/fifteenLvl";
		sixteenLvl = GetPath() + "/sixteenLvl";
		seventeenLvl = GetPath() + "/seventeenLvl";
		eighteenLvl = GetPath() + "/eighteenLvl";
		nineteenLvl = GetPath() + "/nineteenLvl";
		twentyLvl = GetPath() + "/twentyLvl";
		twentyoneLvl = GetPath() + "/twentyoneLvl";
		twentytwoLvl = GetPath() + "/twentytwoLvl";
		twentythreeLvl = GetPath() + "/twentythreeLvl";
		twentyfourLvl = GetPath() + "/twentyfourLvl";
		twentyfiveLvl = GetPath() + "/twentyfiveLvl";
		
		pause=false;
		startPos=0;
	}
	Vector3 ReadPosition(int k) // уже ненужная функция чтения расположения конусов сохраненной карты
	{
		Vector3 tmp;
		using (StreamReader sr = File.OpenText(path)) 
        {
            string s = "";
			int count=1;
            while ((s = sr.ReadLine()) != null) 
            {
				if(count==k+1) x1=float.Parse(s);
				if(count==k+2) y1=float.Parse(s);
				if(count==k+3) z1=float.Parse(s);
				count++;
            }
			tmp=new Vector3(x1, y1, z1);
			sr.Close();
        }
		return tmp;
	}
	void Update () 
	{
		timeReverse += Time.deltaTime;
		if(toggleTxt)
		{
			this.GetComponent<AudioSource>().volume=0;
		}
		else 
		{
			this.GetComponent<AudioSource>().volume=0.7f;
		}
		if(firstCar)
		{
			Car = GameObject.Find("Car Ferrari 458 Italia N020512");
		}
		if(secondCar)
		{
			Car = GameObject.Find("Car Ferrari 458 Italia N020512");
		}
		if(thirdCar)
		{
			Car = GameObject.Find("Car Ferrari 458 Italia N020513");
		}
		if(timeSizeSave==true && !Application.isLoadingLevel)			//задержка перед построением конусов при увеличении/уменьшении карты
		{
			buildMap=false;
			timeSizeSave=false;
		}
		
		if(freeMode && !buildMap)			//строим конусы из сохраненного файла
		{
			if (File.Exists(path))
			{
				for(int j=1; j<76; j+=4)
				{
					if(GameObject.Find("roadblock low"+roadCount)==null){}
					else
					{
						GameObject.Find("roadblock low"+roadCount).transform.position=ReadPosition(j);
						GameObject.Find("roadblock low"+roadCount).transform.localScale=new Vector3(0.012f,0.012f,0.012f);
						roadCount++;
					}
				}
			}
			buildMap=true;
		}
		
		if(Input.GetMouseButton(0) && go)			//если нажали на конус и начали строить карту и сохранять координаты в темп файл...
		{
			coneFin=true;
			coneTime+=Time.deltaTime;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        	   RaycastHit hit; 
        	   if (Physics.Raycast(ray, out hit, Mathf.Infinity)) 
        	   {    
					if(GameObject.Find("roadblock low"+i)==null){}			//если конусы кончились пропускаем
					else
					{
						tapLast=new Vector3 (hit.point.x, startPos, hit.point.z);
        	       		GameObject.Find("roadblock low"+i).transform.position = tapLast;
						GameObject.Find("roadblock low"+i).transform.localScale=new Vector3(0.012f,0.012f,0.012f);
						if(coneTime>0.05f){ i++; coneTime=0;}
						if (!File.Exists(tmpPath))
						{
        					using (sw = File.CreateText(tmpPath))
        					{
        					    sw.WriteLine(i);
								sw.WriteLine(hit.point.x);
								sw.WriteLine(startPos);
								sw.WriteLine(hit.point.z);
        					}
						}
						else
						{
							using (sw = File.AppendText(tmpPath)) 
        					{
        					    sw.WriteLine(i);
								sw.WriteLine(hit.point.x);
								sw.WriteLine(startPos);
								sw.WriteLine(hit.point.z);
        					}
						}
					}	
        	   } 
		}
		else if(coneFin)
		{
			go=false;
			coneFin=false;
		}
		
		if (Input.touchCount > 0)  //Swipe section
        {
            Touch touch = Input.touches[0];
            switch (touch.phase) 
            {
                case TouchPhase.Began:

                    lastSwipe = MainMenu.SwipeDirection.None;
                    lastSwipeTime = 0;
                    couldBeSwipe = true;
                    swipeStartPos = touch.position;
                    startTime = Time.time;
                    break;

                case TouchPhase.Moved:

                    if (Mathf.Abs(touch.position.y - swipeStartPos.y) > comfortZone) 
                    {
                        couldBeSwipe = false;
                    }
                    break;

                case TouchPhase.Ended:
				{
                    if (couldBeSwipe)
                    {
                        float swipeTime = Time.time - startTime;
                        float swipeDist = (new Vector3(0, touch.position.x, 0) - new Vector3(0, swipeStartPos.x, 0)).magnitude;
                        if ((swipeTime < maxSwipeTime) && (swipeDist > minSwipeDist)) 
                        {
                            float swipeValue = Mathf.Sign(touch.position.x - swipeStartPos.x);
							mySwipe=true;
                            if (swipeValue > 0) lastSwipe = MainMenu.SwipeDirection.Up;
                            else if (swipeValue < 0) lastSwipe = MainMenu.SwipeDirection.Down;
                            lastSwipeTime = Time.time;
                        }
                    }
                    break;
				}
				default: mySwipe=false;break;
            }
        }         //end swipe section
		else mySwipe=false;
	}
	
	string GetPath()
	{
		string root = Application.dataPath.Substring(0, Application.dataPath.Length - 13) + "/Documents";
		#if UNITY_EDITOR
		root = Application.dataPath.Substring(0, Application.dataPath.Length);
		#endif
        return root;
	}
	
	void OnGUI()
	{
		if(Application.isLoadingLevel)
		{
			GUI.skin = scaleGUI==2? textStyle : textStyle2;
			GUI.TextField(new Rect (480*scaleGUI,345*scaleGUI,100*scaleGUI,30*scaleGUI), "Loading...", 15);
			return;
		}
		if(!gameMode)			//меню
		{
    		if(!optionsMode && !careerMode && !howToPlayMode)
			{                               
 				if(firstLoad)
				{
					Instantiate(mainScreenGO);
					firstLoad=false;
				}
			}
			else if(optionsMode) //опции
			{    
				if(firstLoad)
				{
					Instantiate(loaderOPT);
					firstLoad=false;
				}
				GUI.skin= scaleGUI==2? backSkin : backSkin2;
    			if(GUI.Button(new Rect(50*scaleGUI, 50*scaleGUI, 99.5f*scaleGUI, 57*scaleGUI), ""))//Back
				{
    			    optionsMode = false;
					optionsModeOnGame = false;
					firstLoad=true;
					OptionsLoader.bundle.Unload(true);
    			}
				/*GUI.skin= scaleGUI==2? restorePurSkin : restorePurSkin2;
    			if(GUI.Button(restorePurRect,""))//Restore Purchases
				{
    				_RestorePurchases();
				}*/
    		}
			else if(howToPlayMode) //как играть
			{	
				if(firstLoad)
				{
					Instantiate(loaderHTP); //загрузка картинок
					firstLoad=false;
				}
				GUI.skin= scaleGUI==2? backSkin : backSkin2;
				if(GUI.Button(new Rect(50*scaleGUI, 50*scaleGUI, 99.5f*scaleGUI, 57*scaleGUI), ""))//Back
				{
					firstLoad=true;
    			    howToPlayMode=false;
					HowToPlayLoader.bundle.Unload(true);
    			}
				GUI.skin= scaleGUI==2? saveMarkerSkin : saveMarkerSkin2;
   	 			if(GUI.Button(saveMarkerRect,""))//Save Marker
				{
					
				}
			}
			else if(careerMode) //когда нажали кнопку карьера
			{
				if(carChoiseMod) //меню выбора машины
				{
					if(firstLoad) //загрузка 
					{
						Instantiate(loaderCH);
						firstLoad=false;
					}
					/*if(mySwipe && lastSwipe == MainMenu.SwipeDirection.Up)
					{
						if(carCount==3) carCount=0;
						carCount++;
					}
					else if(mySwipe && lastSwipe == MainMenu.SwipeDirection.Down)
					{
						if(carCount==1) carCount=4;
						carCount--;
					}*/
					if(carCount==3)//2 car 
					{
    		    	    secondCar = true;
						firstCar = false;
						thirdCar = false;
    		    	}
					if(carCount==2)//3 car
					{
    		    	    thirdCar = true;
						firstCar = false;
						secondCar = false;;
    		    	}
					if(nextBackButton) //WTF? видимо костыль от стаса, чтоб появлялись все картики после загрузки, а не по мере поступления
					{
						GUI.skin= scaleGUI==2? backSkin : backSkin2;
						if(GUI.Button(new Rect(50*scaleGUI, 50*scaleGUI, 99.5f*scaleGUI, 57*scaleGUI), ""))//Back
						{
							GameObject.Find("Plane").renderer.material = mainMaterial;
  							careerMode = false;
							trackChoiseMod = false;
							carChoiseMod=false;
							firstLoad=true;
							CarChoiseLoader.bundle.Unload(true);
							nextBackButton=false;
  						}
						GUI.skin= scaleGUI==2? nextSkin : nextSkin2;
						if(GUI.Button(new Rect(850*scaleGUI, 50*scaleGUI, 99.5f*scaleGUI, 57*scaleGUI), ""))//Next
						{
							GameObject.Find("Plane").renderer.material = mainMaterial;
							trackChoiseMod = true;
							carChoiseMod=false;
							firstLoad=true;
							CarChoiseLoader.bundle.Unload(true);
							nextBackButton=false;
  						}
					}
				}
				if(trackChoiseMod) //выбор трасс
				{
					if(firstLoad) //загрузка картин
					{
						Instantiate(loaderGO);
						firstLoad=false;
					}
					if(loadMapCount>49 && !isPurchased)//>5
					{
						GUI.skin=buySkin;
    					if(GUI.Button(buyRect,""))//Buy
						{
    						
    					}
					}
					else{}
					GUI.skin= scaleGUI==2? firstMapSkin : firstMapSkin2;
					if(GUI.Button(firstMapRect," ")) //1
					{  		
    				}
				}
			}
		}
		else
		{
			if(!isScoreLevel && !pauseOnGameMode && !optionsModeOnGame)
			{
				if(freeMode) // забритые кнопки фримода
				{
					/*GUI.skin = coneSkin;
					if(GUI.Button(incremRect,"")) //cone
					{
						i++;
						go=true;
					} 
					GUI.skin = customSkin;
					if(GUI.Button(saveRect,"Save"))//Save
					{
					    if (File.Exists(tmpPath))
						{
							File.Copy(tmpPath, path, true);
						}
						else
						{
							File.Copy(clearPath, path, true);
						}
					}*/
				}
				GUI.skin= scaleGUI==2? pauseSkin : pauseSkin2;
				if(GUI.Button(pauseRect, " "))//pause
				{
					pauseOnGameMode=true;
    			}
				GUI.skin= scaleGUI==2? RestartSkin : RestartSkin2;
				if(GUI.Button(RestartRect," "))//Restart
				{
					File.Delete(tmpPath);
					i=0;
				    Application.LoadLevel(Application.loadedLevelName);
				}
				
				/*GUI.skin = customSkin;
				if(GUI.Button(sizeMaxRect,"Size++"))//Size++
				{
					isMiniMap=false;
					string str = Application.loadedLevelName;
					str = str.Remove(str.Length - 4, 4);
					roadCount=1;
					i=0;
					if (File.Exists(tmpPath))
					{
						File.Delete(tmpPath);
					}
				    Application.LoadLevel(str);
					timeSizeSave=true;
				}
				if(GUI.Button(sizeMinRect,"Size--"))//Size--
				{
					isMiniMap=true;
					roadCount=1;
					i=0;
					if (File.Exists(tmpPath))
					{
						File.Delete(tmpPath);
					}
				    Application.LoadLevel(Application.loadedLevelName+"Mini");
					timeSizeSave=true;
				}*/
				if(!Application.isLoadingLevel)CalcSpeed();
				if(timeReverse>0.5f) inversionReload=inverse;
			}
			else if(isScoreLevel) //меню показа очков
			{
				if(firstLoad) //загрузка картинок
				{
					Instantiate(loaderRES);
					firstLoad=false;
				}
			}
			else if(pauseOnGameMode) //пауза в игре
			{
				GUI.skin= scaleGUI==2? fonOnGameStyle : fonOnGameStyle2;
				GUI.Label(fonOnGameRect, "");
				GUI.skin= scaleGUI==2? backOnGameStyle : backOnGameStyle2;    
    	    	if(GUI.Button(backOnGameRect, ""))
				{
					pauseOnGameMode=false;
    	    	}     
				GUI.skin= scaleGUI==2? menuOnGameStyle : menuOnGameStyle2;
				if(GUI.Button(menuOnGameRect, ""))
				{
					gameMode = false;
					freeMode=false;
					pauseOnGameMode=false;
					firstLoad=true;
					Pause ();
    	    	}   
				GUI.skin= scaleGUI==2? optionSkin : optionSkin2;
    	    	if(GUI.Button(optionsRect,""))//Options
				{
					pauseOnGameMode=false;
    	    	    optionsModeOnGame = true;
    	    	}
			}
			else if(optionsModeOnGame) //опции в игре
			{
				if(firstLoad) // загрузка картинок
				{
					Instantiate(loaderOPT);
					firstLoad=false;
				}
				GUI.skin= scaleGUI==2? backSkin : backSkin2;
    			if(GUI.Button(new Rect(50*scaleGUI, 50*scaleGUI, 99.5f*scaleGUI, 57*scaleGUI), ""))//Back
				{
    			    optionsMode = false;
					optionsModeOnGame = false;
					firstLoad=true;
					OptionsLoader.bundle.Unload(true);
    			}
				/*GUI.skin= scaleGUI==2? restorePurSkin : restorePurSkin2;
    			if(GUI.Button(restorePurRect,""))//Restore Purchases
				{
    				_RestorePurchases();
				}*/
				GUI.skin= scaleGUI==2? fonOnGameStyle : fonOnGameStyle2;
				GUI.Label(fonOnGameRect, "");
			}
		}
	}
	
	public void Pause()
	{
		pause=true;
		buildMap=false;
		roadCount=1;
		Application.LoadLevel("QCAR-TempMenu");
	}
	public void UnPause()
	{
		pause=false;
	}
	
	public bool IsUnlockLvl(int checkTrack) //проверка пройден уровень или нет
	{
		if(checkTrack==1 && File.Exists(firstLvl)) return true;
		if(checkTrack==3 && File.Exists(secondLvl)) return true;
		if(checkTrack==5 && File.Exists(thirdLvl)) return true;
		if(checkTrack==7 && File.Exists(fourthLvl)) return true;
		if(checkTrack==9 && File.Exists(fifthLvl)) return true;
		if(checkTrack==11 && File.Exists(sixthLvl)) return true;
		if(checkTrack==13 && File.Exists(seventhLvl)) return true;
		if(checkTrack==15 && File.Exists(eighthLvl)) return true;
		if(checkTrack==17 && File.Exists(nineLvl)) return true;
		if(checkTrack==19 && File.Exists(tenLvl)) return true;
		if(checkTrack==21 && File.Exists(elevenLvl)) return true;
		if(checkTrack==23 && File.Exists(twelveLvl)) return true;
		if(checkTrack==25 && File.Exists(thirteenLvl)) return true;
		if(checkTrack==27 && File.Exists(fourteenLvl)) return true;
		if(checkTrack==29 && File.Exists(fifteenLvl)) return true;
		if(checkTrack==31 && File.Exists(sixteenLvl)) return true;
		if(checkTrack==33 && File.Exists(seventeenLvl)) return true;
		if(checkTrack==35 && File.Exists(eighteenLvl)) return true;
		if(checkTrack==37 && File.Exists(nineteenLvl)) return true;
		if(checkTrack==39 && File.Exists(twentyLvl)) return true;
		if(checkTrack==41 && File.Exists(twentyoneLvl)) return true;
		if(checkTrack==43 && File.Exists(twentytwoLvl)) return true;
		if(checkTrack==45 && File.Exists(twentythreeLvl)) return true;
		if(checkTrack==47 && File.Exists(twentyfourLvl)) return true;
		if(checkTrack==49 && File.Exists(twentyfiveLvl)) return true;
		return false;  //false 26-02 stas
	}
	public void SetGUISkins(Texture2D sTexture) //меняет картинки карт
	{
		firstMapSkin.button.normal.background   = sTexture;
		firstMapSkin.button.active.background   = sTexture;
		firstMapSkin.button.hover.background    = sTexture;
		firstMapSkin.button.focused.background  = sTexture;
		firstMapSkin.button.onNormal.background = sTexture;
		firstMapSkin.button.onActive.background = sTexture;
		firstMapSkin.button.onHover.background  = sTexture;
		firstMapSkin.button.onFocused.background= sTexture;
		
		firstMapSkin2.button.normal.background   = sTexture;
		firstMapSkin2.button.active.background   = sTexture;
		firstMapSkin2.button.hover.background    = sTexture;
		firstMapSkin2.button.focused.background  = sTexture;
		firstMapSkin2.button.onNormal.background = sTexture;
		firstMapSkin2.button.onActive.background = sTexture;
		firstMapSkin2.button.onHover.background  = sTexture;
		firstMapSkin2.button.onFocused.background= sTexture;
	}
	void CalcSpeed() 
	{
		/*if(Car==null){}
		else
		{
			Joystick moveTouchPad = GameObject.Find("Forward").GetComponent("Joystick") as Joystick;
			speed=Vector3.Dot (Car.rigidbody.velocity, Car.transform.forward);
			speed=Math.Abs(speed);
			strSpeed=System.Convert.ToString(speed);
			GUI.skin=customSkin;
        	GUI.TextField(new Rect(20*scaleGUI, 120*scaleGUI, 100*scaleGUI, 30*scaleGUI), moveTouchPad.position.y>0.01? strSpeed:"0", 25);
		}//*/
    }
	public string GetBeetTime() //наше среднее время
	{
		if(loadMapCount==1) //1
		{
			return "7.49";
		}
		if(loadMapCount==3) //2
		{
			return "7.14";
		}
		if(loadMapCount==5) //3
		{
			return "22.46";
		}
		if(loadMapCount==7) //4
		{
			return "42.10";
		}
		if(loadMapCount==9) //5
		{
			return "12.91";
		}
		if(loadMapCount==11) //6
		{
			return "21.56";
		}
		if(loadMapCount==13) //7
		{
			return "14.32";
		}
		if(loadMapCount==15) //8
		{
			return "48.20";
		}
		if(loadMapCount==17) //9
		{
			return "38.68";
		}
		if(loadMapCount==19) //10
		{
			return "46.06";
		}
		if(loadMapCount==21) //11
		{
			return "56.74";
		}
		if(loadMapCount==23) //12
		{
			return "42.11";
		}
		if(loadMapCount==25) //13
		{
			return "55.21";
		}
		if(loadMapCount==27) //14
		{
			return "17.55";
		}
		if(loadMapCount==29) //15
		{
			return "38.97";
		}
		if(loadMapCount==31) //16
		{
			return "38.97";
		}
		if(loadMapCount==33) //17
		{
			return "38.97";
		}
		if(loadMapCount==35) //18
		{
			return "38.97";
		}
		if(loadMapCount==37) //19
		{
			return "38.97";
		}
		if(loadMapCount==39) //20
		{
			return "38.97";
		}
		if(loadMapCount==41) //21
		{
			return "38.97";
		}
		if(loadMapCount==43) //22
		{
			return "38.97";
		}
		if(loadMapCount==45) //23
		{
			return "38.97";
		}
		if(loadMapCount==47) //24
		{
			return "38.97";
		}
		if(loadMapCount==49) //25
		{
			return "38.97";
		}
		return "0";
	}
	public string GetGoalDriftScore() //наши среднии очки
	{
		if(loadMapCount==1) //1
		{
			return "103";
		}
		if(loadMapCount==3) //2
		{
			return "87";
		}
		if(loadMapCount==5) //3
		{
			return "444";
		}
		if(loadMapCount==7) //4
		{
			return "719";
		}
		if(loadMapCount==9) //5
		{
			return "112";
		}
		if(loadMapCount==11) //6
		{
			return "302";
		}
		if(loadMapCount==13) //7
		{
			return "206";
		}
		if(loadMapCount==15) //8
		{
			return "761";
		}
		if(loadMapCount==17) //9
		{
			return "593";
		}
		if(loadMapCount==19) //10
		{
			return "742";
		}
		if(loadMapCount==21) //11
		{
			return "822";
		}
		if(loadMapCount==23) //12
		{
			return "683";
		}
		if(loadMapCount==25) //13
		{
			return "866";
		}
		if(loadMapCount==27) //14
		{
			return "223";
		}
		if(loadMapCount==29) //15
		{
			return "643";
		}
		if(loadMapCount==31) //16
		{
			return "643";
		}
		if(loadMapCount==33) //17
		{
			return "643";
		}
		if(loadMapCount==35) //18
		{
			return "643";
		}
		if(loadMapCount==37) //19
		{
			return "643";
		}
		if(loadMapCount==39) //20
		{
			return "643";
		}
		if(loadMapCount==41) //21
		{
			return "643";
		}
		if(loadMapCount==43) //22
		{
			return "643";
		}
		if(loadMapCount==45) //23
		{
			return "643";
		}
		if(loadMapCount==47) //24
		{
			return "643";
		}
		if(loadMapCount==49) //25
		{
			return "643";
		}
		return "0";
	}
	public string GetYourTime() //время за которое пршли трассу
	{
		using (StreamReader sr = File.OpenText(GetThisLvlDir())) 
        {
			double driftTime;
            string s = "";
			s = sr.ReadLine();
			s = sr.ReadLine();
			driftTime=double.Parse(s);
			sr.Close();
			return System.Convert.ToString(driftTime);
        }
	}
	public string GetYourDriftScore() //очки за трассу
	{
		using (StreamReader sr = File.OpenText(GetThisLvlDir())) 
        {
			int driftTime;
            string s = "";
			s = sr.ReadLine();
			driftTime=int.Parse(s);
			sr.Close();
			return System.Convert.ToString(driftTime);
        }
	}
	public string GetThisLvlDir() //возвращает файл с настройками текущей трассы
	{
		if(loadMapCount==1) //1
		{
			return firstLvl;
		}
		if(loadMapCount==3) //2
		{
			return secondLvl;
		}
		if(loadMapCount==5) //3
		{
			return thirdLvl;
		}
		if(loadMapCount==7) //4
		{
			return fourthLvl;
		}
		if(loadMapCount==9) //5
		{
			return fifthLvl;
		}
		if(loadMapCount==11) //6
		{
			return sixthLvl;
		}
		if(loadMapCount==13) //7
		{
			return seventhLvl;
		}
		if(loadMapCount==15) //8
		{
			return eighthLvl;
		}
		if(loadMapCount==17) //9
		{
			return nineLvl;
		}
		if(loadMapCount==19) //10
		{
			return tenLvl;
		}
		if(loadMapCount==21) //11
		{
			return elevenLvl;
		}
		if(loadMapCount==23) //12
		{
			return twelveLvl;
		}
		if(loadMapCount==25) //13
		{
			return thirteenLvl;
		}
		if(loadMapCount==27) //14
		{
			return fourteenLvl;
		}
		if(loadMapCount==29) //15
		{
			return fifteenLvl;
		}
		if(loadMapCount==31) //16
		{
			return sixteenLvl;
		}
		if(loadMapCount==33) //17
		{
			return seventeenLvl;
		}
		if(loadMapCount==35) //18
		{
			return eighteenLvl;
		}
		if(loadMapCount==37) //19
		{
			return nineteenLvl;
		}
		if(loadMapCount==39) //20
		{
			return twentyLvl;
		}
		if(loadMapCount==41) //21
		{
			return twentyoneLvl;
		}
		if(loadMapCount==43) //22
		{
			return twentytwoLvl;
		}
		if(loadMapCount==45) //23
		{
			return twentythreeLvl;
		}
		if(loadMapCount==47) //24
		{
			return twentyfourLvl;
		}
		if(loadMapCount==49) //25
		{
			return twentyfiveLvl;
		}
		return "0";
	}
	void SetGUIScale() // устанавливает положение рект
	{
		menuOnGameRect= new Rect(390*scaleGUI,345*scaleGUI,235*scaleGUI,80*scaleGUI);
		backOnGameRect= new Rect(390*scaleGUI,245*scaleGUI,235*scaleGUI,80*scaleGUI);	
		fonOnGameRect = new Rect(0*scaleGUI,0*scaleGUI,1024*scaleGUI,768*scaleGUI);
		RestartRect   = new Rect(20*scaleGUI,20*scaleGUI,78*scaleGUI,78*scaleGUI);
		optionsRect   = new Rect(390*scaleGUI,445*scaleGUI,235*scaleGUI,80*scaleGUI);
		pauseRect     = new Rect(20*scaleGUI, 120*scaleGUI, 78*scaleGUI, 78*scaleGUI);
		buyRect      = new Rect(465*scaleGUI,600*scaleGUI,84*scaleGUI,57*scaleGUI);
		restorePurRect= new Rect(380*scaleGUI,500*scaleGUI,267*scaleGUI,80*scaleGUI);	
		firstMapRect   = new Rect(360*scaleGUI, 230*scaleGUI, 300*scaleGUI, 300*scaleGUI);
		saveMarkerRect 	 = new Rect(260*scaleGUI,600*scaleGUI,100*scaleGUI,57*scaleGUI);
	}
}
