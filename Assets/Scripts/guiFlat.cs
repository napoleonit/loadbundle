using UnityEngine;
using System.Collections;

public class guiFlat : MonoBehaviour 
{
	public GameObject[] allPol;
	public GameObject[] allWall;
	
	public GUISkin EmptySkin;
	
	public GUISkin choiseRoomSkin;
	public GUISkin choiseRoom1Skin;
	public GUISkin choiseRoom2Skin;
	public GUISkin choiseRoom3Skin;
	public GUISkin choiseRoom4Skin;
	public GUISkin choiseRoomClick1Skin;
	public GUISkin choiseRoomClick2Skin;
	public GUISkin choiseRoomClick3Skin;
	public GUISkin choiseRoomClick4Skin;
	public GUISkin choiseRoomFoneSkin;

	public GUISkin choiseWallSkin;
	public GUISkin choiseWall1Skin;
	public GUISkin choiseWall2Skin;
	public GUISkin choiseWall3Skin;
	public GUISkin choiseWallFoneSkin;

	public GUISkin choiseFloorSkin;
	public GUISkin choiseFloor1Skin;
	public GUISkin choiseFloor2Skin;
	public GUISkin choiseFloor3Skin;
	public GUISkin choiseFloor4Skin;
	public GUISkin choiseFloorFoneSkin;
	
	public GUISkin hideFurnitSkin;
	public GUISkin choiseDoorSkin;
	public GUISkin podkladka2CellSkin;
	//-------------------------------------
	public GUISkin choiseRoomSkin2;
	public GUISkin choiseRoom1Skin2;
	public GUISkin choiseRoom2Skin2;
	public GUISkin choiseRoom3Skin2;
	public GUISkin choiseRoom4Skin2;
	public GUISkin choiseRoomClick1Skin2;
	public GUISkin choiseRoomClick2Skin2;
	public GUISkin choiseRoomClick3Skin2;
	public GUISkin choiseRoomClick4Skin2;
	public GUISkin choiseRoomFoneSkin2;

	public GUISkin choiseWallSkin2;
	public GUISkin choiseWall1Skin2;
	public GUISkin choiseWall2Skin2;
	public GUISkin choiseWall3Skin2;
	public GUISkin choiseWallFoneSkin2;

	public GUISkin choiseFloorSkin2;
	public GUISkin choiseFloor1Skin2;
	public GUISkin choiseFloor2Skin2;
	public GUISkin choiseFloor3Skin2;
	public GUISkin choiseFloor4Skin2;
	public GUISkin choiseFloorFoneSkin2;
	
	public GUISkin hideFurnitSkin2;
	public GUISkin choiseDoorSkin2;
	public GUISkin podkladka2CellSkin2;
	//--------------------------------------
	public Texture choiseRoomTex;
	public Texture choiseWallTex;
	public Texture choiseFloorTex;
	public Texture choiseDoorTex;
				  
	public Texture choiseRoomTexClick;
	public Texture choiseWallTexClick;
	public Texture choiseFloorTexClick;
	public Texture choiseDoorTexClick;
	//----------------------------------
	public Texture choiseRoomTex2;
	public Texture choiseWallTex2;
	public Texture choiseFloorTex2;
	public Texture choiseDoorTex2;
				  
	public Texture choiseRoomTexClick2;
	public Texture choiseWallTexClick2;
	public Texture choiseFloorTexClick2;
	public Texture choiseDoorTexClick2;
	//---------------------------------------
	public Texture choiseColorTex;
	public Texture choiseColor1Tex;
	public Texture choiseColor2Tex;
	public Texture choiseColor3Tex;
	public Texture choiseColorFoneTex;
	public Texture choiseColorFloor1Tex;
	public Texture choiseColorFloor2Tex;
	public Texture choiseColorFloor3Tex;
	public Texture choiseColorFloor4Tex;
	public Texture choiseColorFloorFoneTex;
	public Texture choiseColorWall1Tex;
	public Texture choiseColorWall2Tex;
	public Texture choiseColorWall3Tex;
	public Texture choiseColorWallFoneTex;
	public Texture choiseColorDoor1Tex;
	public Texture choiseColorDoor2Tex;
	public Texture choiseColorDoorFoneTex;
	
	public Texture TextureFloorLam1;
	public Texture TextureFloorLam2;
	public Texture TextureFloorLin1;
	public Texture TextureFloorLin2;
	public Texture TextureFloorKov1;
	public Texture TextureFloorKov2;	
	public Texture TextureFloorPli1;
	public Texture TextureFloorPli2;
	public Texture TextureWallPan1;
	public Texture TextureWallPan2;
	public Texture TextureWallObo1;
	public Texture TextureWallObo2;
	public Texture TextureWallPli1;
	public Texture TextureWallPli2;
	public Texture TextureDoorVh1;
	public Texture TextureDoorVh2;
	public Texture TextureDoorVn1;
	public Texture TextureDoorVn2;	
	public Texture choiseTextureFoneTex;
	public Texture choiseTextureDoorFoneTex;
	
	public Texture choiseColorTex2;
	public Texture choiseColor1Tex2;
	public Texture choiseColor2Tex2;
	public Texture choiseColor3Tex2;
	public Texture choiseColorFoneTex2;
	public Texture choiseColorFloor1Tex2;
	public Texture choiseColorFloor2Tex2;
	public Texture choiseColorFloor3Tex2;
	public Texture choiseColorFloor4Tex2;
	public Texture choiseColorFloorFoneTex2;
	public Texture choiseColorWall1Tex2;
	public Texture choiseColorWall2Tex2;
	public Texture choiseColorWall3Tex2;
	public Texture choiseColorWallFoneTex2;
	public Texture choiseColorDoor1Tex2;
	public Texture choiseColorDoor2Tex2;
	public Texture choiseColorDoorFoneTex2;
	
	public Texture TextureFloorLam12;
	public Texture TextureFloorLam22;
	public Texture TextureFloorLin12;
	public Texture TextureFloorLin22;
	public Texture TextureFloorKov12;
	public Texture TextureFloorKov22;	
	public Texture TextureFloorPli12;
	public Texture TextureFloorPli22;
	public Texture TextureWallPan12;
	public Texture TextureWallPan22;
	public Texture TextureWallObo12;
	public Texture TextureWallObo22;
	public Texture TextureWallPli12;
	public Texture TextureWallPli22;
	public Texture TextureDoorVh12;
	public Texture TextureDoorVh22;
	public Texture TextureDoorVn12;
	public Texture TextureDoorVn22;	
	public Texture choiseTextureFoneTex2;
	public Texture choiseTextureDoorFoneTex2;
	//-------------------------------------------
	public Texture lam1Tex;
	public Texture lam2Tex;
	public Texture lin1Tex;
	public Texture lin2Tex;
	public Texture kov1Tex;
	public Texture kov2Tex;
	public Texture pli1Tex;
	public Texture pli2Tex;
	
	public Texture pan1Tex;
	public Texture pan2Tex;
	public Texture obo1Tex;
	public Texture obo2Tex;
	public Texture obo3Tex;
	public Texture wallPli1Tex;
	public Texture wallPli2Tex;
	//-------------------------------------------
	Rect choiseColorRect;
	Rect choiseColor1Rect;
	Rect choiseColor2Rect;
	Rect choiseColor3Rect;
	Rect choiseColorFoneRect;
	Rect choiseColorFloor1Rect;
	Rect choiseColorFloor2Rect;
	Rect choiseColorFloor3Rect;
	Rect choiseColorFloor4Rect;
	Rect choiseColorFloorFoneRect;
	Rect choiseColorWall1Rect;
	Rect choiseColorWall2Rect;
	Rect choiseColorWall3Rect;
	Rect choiseColorWallFoneRect;
	Rect choiseColorDoor1Rect;
	Rect choiseColorDoor2Rect;
	Rect choiseColorDoorFoneRect;
	
	Rect TextureFloorLam1Rect;
	Rect TextureFloorLam2Rect;
	Rect TextureFloorLin1Rect;
	Rect TextureFloorLin2Rect;
	Rect TextureFloorKov1Rect;
	Rect TextureFloorKov2Rect;	
	Rect TextureFloorPli1Rect;
	Rect TextureFloorPli2Rect;
	Rect TextureWallPan1Rect;
	Rect TextureWallPan2Rect;
	Rect TextureWallObo1Rect;
	Rect TextureWallObo2Rect;
	Rect TextureWallPli1Rect;
	Rect TextureWallPli2Rect;
	Rect TextureDoorVh1Rect;
	Rect TextureDoorVh2Rect;
	Rect TextureDoorVn1Rect;
	Rect TextureDoorVn2Rect;	
	Rect choiseTextureFoneTexRect;
	Rect choiseTextureDoorFoneTexRect;
	Rect firstTexture;
	Rect secondTexture;
	//-------------------------------------------
	Rect choiseRoomRect;
	Rect choiseRoom1Rect;
	Rect choiseRoom2Rect;
	Rect choiseRoom3Rect;
	Rect choiseRoom4Rect;
	Rect choiseRoomFoneRect;

	Rect choiseWallRect;
	Rect choiseWall1Rect;
	Rect choiseWall2Rect;
	Rect choiseWall3Rect;
	Rect choiseWallFoneRect;

	Rect choiseFloorRect;
	Rect choiseFloor1Rect;
	Rect choiseFloor2Rect;
	Rect choiseFloor3Rect;
	Rect choiseFloor4Rect;
	Rect choiseFloorFoneRect;

	Rect hideFurnitRect;
	Rect choiseDoorRect;
	Rect podkladka2CellRect;
	//----------------------------Main Button
	bool choiseRoomBtn = false;
	bool choiseWallBtn = false;
	bool choiseFloorBtn = false;
	bool choiseDoorBtn = false;
	bool choiseColorBtn = false;
	//----------------------------Choise Room Button
	bool choiseRoom1Btn = false;
	bool choiseRoom2Btn = false;
	bool choiseRoom3Btn = false;
	bool choiseRoom4Btn = false;
	//----------------------------Choise Wall Button
	bool choiseWall1Btn = false;
	bool choiseWall2Btn = false;
	bool choiseWall3Btn = false;
	//----------------------------Choise Floor Button
	bool choiseFloor1Btn = false;
	bool choiseFloor2Btn = false;
	bool choiseFloor3Btn = false;
	bool choiseFloor4Btn = false;
	//----------------------------Hide Button
	bool choiseHide1Btn = true;
	bool choiseHide2Btn = true;
	bool choiseHide3Btn = true;
	bool choiseHide4Btn = true;
	//----------------------------Choise Color Button
	bool choiseColor1Btn = false;
	bool choiseColor2Btn = false;
	bool choiseColor3Btn = false;
	//----------------------------Choise Texture Button
	bool choiseTexLamBtn = false;
	bool choiseTexLinBtn = false;
	bool choiseTexKovBtn = false;
	bool choiseTexPliBtn = false;
	bool choiseTexPanBtn = false;
	bool choiseTexOboBtn = false;
	bool choiseTexWallPliBtn = false;
	bool choiseTexVhBtn = false;
	bool choiseTexVnBtn = false;
	
	int scaleGUI;
	// Use this for initialization
	void Start () 
	{
		scaleGUI = Screen.width/1024;
		
		choiseColorRect    = new Rect(1800*scaleGUI/2, 700*scaleGUI/2, 168*scaleGUI/2, 168*scaleGUI/2);
		choiseColor1Rect   = new Rect(1573*scaleGUI/2, 857*scaleGUI/2, 390*scaleGUI/2, 95*scaleGUI/2);
		choiseColor2Rect   = new Rect(1573*scaleGUI/2, 952*scaleGUI/2, 390*scaleGUI/2, 95*scaleGUI/2);
		choiseColor3Rect   = new Rect(1573*scaleGUI/2, 1043*scaleGUI/2, 390*scaleGUI/2, 95*scaleGUI/2);
		choiseColorFoneRect= new Rect(1573*scaleGUI/2, 857*scaleGUI/2, 390*scaleGUI/2, 387*scaleGUI/2);
		
		choiseColorFloor1Rect = new Rect(1230*scaleGUI/2, 857*scaleGUI/2, 296*scaleGUI/2, 70*scaleGUI/2);
		choiseColorFloor2Rect = new Rect(1230*scaleGUI/2, 924*scaleGUI/2, 296*scaleGUI/2, 70*scaleGUI/2);
		choiseColorFloor3Rect = new Rect(1230*scaleGUI/2, 994*scaleGUI/2, 296*scaleGUI/2, 70*scaleGUI/2);
		choiseColorFloor4Rect = new Rect(1230*scaleGUI/2, 1064*scaleGUI/2, 296*scaleGUI/2, 70*scaleGUI/2);
		choiseColorFloorFoneRect = new Rect(1230*scaleGUI/2, 857*scaleGUI/2, 314*scaleGUI/2, 290*scaleGUI/2);
		choiseColorWall1Rect  = new Rect(1230*scaleGUI/2, 857*scaleGUI/2, 296*scaleGUI/2, 70*scaleGUI/2);
		choiseColorWall2Rect  = new Rect(1230*scaleGUI/2, 924*scaleGUI/2, 296*scaleGUI/2, 70*scaleGUI/2);
		choiseColorWall3Rect  = new Rect(1230*scaleGUI/2, 994*scaleGUI/2, 296*scaleGUI/2, 70*scaleGUI/2);
		choiseColorWallFoneRect = new Rect(1230*scaleGUI/2, 857*scaleGUI/2, 313*scaleGUI/2, 220*scaleGUI/2);
		choiseColorDoor1Rect  = new Rect(1230*scaleGUI/2, 857*scaleGUI/2, 296*scaleGUI/2, 70*scaleGUI/2);
		choiseColorDoor2Rect  = new Rect(1230*scaleGUI/2, 924*scaleGUI/2, 296*scaleGUI/2, 70*scaleGUI/2);
		choiseColorDoorFoneRect = new Rect(1230*scaleGUI/2, 857*scaleGUI/2, 313*scaleGUI/2, 151*scaleGUI/2);
		
		choiseTextureFoneTexRect = new Rect(1040*scaleGUI/2, 857*scaleGUI/2, 176*scaleGUI/2, 331*scaleGUI/2);
		choiseTextureDoorFoneTexRect = new Rect(900*scaleGUI/2, 857*scaleGUI/2, 309*scaleGUI/2, 343*scaleGUI/2);
		TextureDoorVh1Rect = new Rect(1000*scaleGUI/2, 870*scaleGUI/2, 168*scaleGUI/2, 168*scaleGUI/2);
		TextureDoorVh2Rect = new Rect(1080*scaleGUI/2, 870*scaleGUI/2, 168*scaleGUI/2, 168*scaleGUI/2);
		
		firstTexture = new Rect(1065*scaleGUI/2, 892*scaleGUI/2, 130*scaleGUI/2, 130*scaleGUI/2);
		secondTexture = new Rect(1065*scaleGUI/2, 1027*scaleGUI/2, 130*scaleGUI/2, 130*scaleGUI/2);
		
		
		choiseRoomRect     = new Rect(30*scaleGUI/2, 50*scaleGUI/2, 168*scaleGUI/2, 168*scaleGUI/2);
		choiseRoom1Rect    = new Rect(36*scaleGUI/2, 210*scaleGUI/2, 389*scaleGUI/2, 95*scaleGUI/2);
		choiseRoom2Rect    = new Rect(36*scaleGUI/2, 305*scaleGUI/2, 389*scaleGUI/2, 95*scaleGUI/2); 
		choiseRoom3Rect    = new Rect(36*scaleGUI/2, 400*scaleGUI/2, 389*scaleGUI/2, 95*scaleGUI/2);
		choiseRoom4Rect    = new Rect(36*scaleGUI/2, 495*scaleGUI/2, 389*scaleGUI/2, 95*scaleGUI/2);
		choiseRoomFoneRect = new Rect(36*scaleGUI/2, 210*scaleGUI/2, 389*scaleGUI/2, 387*scaleGUI/2);
									 
		choiseWallRect     = new Rect(30*scaleGUI/2, 1000*scaleGUI/2, 168*scaleGUI/2, 168*scaleGUI/2);
		choiseWall1Rect    = new Rect(36*scaleGUI/2, 1160*scaleGUI/2, 389*scaleGUI/2, 95*scaleGUI/2); 
		choiseWall2Rect    = new Rect(36*scaleGUI/2, 1255*scaleGUI/2, 389*scaleGUI/2, 95*scaleGUI/2);
		choiseWall3Rect    = new Rect(36*scaleGUI/2, 1350*scaleGUI/2, 389*scaleGUI/2, 95*scaleGUI/2);
		choiseWallFoneRect = new Rect(36*scaleGUI/2, 1160*scaleGUI/2, 389*scaleGUI/2, 291*scaleGUI/2);
		podkladka2CellRect = new Rect(36*scaleGUI/2, 1160*scaleGUI/2, 389*scaleGUI/2, 190*scaleGUI/2);
		
		choiseFloorRect    = new Rect(1800*scaleGUI/2, 50*scaleGUI/2, 168*scaleGUI/2, 168*scaleGUI/2);
		choiseFloor1Rect   = new Rect(1580*scaleGUI/2, 210*scaleGUI/2, 390*scaleGUI/2, 95*scaleGUI/2);
		choiseFloor2Rect   = new Rect(1580*scaleGUI/2, 305*scaleGUI/2, 390*scaleGUI/2, 95*scaleGUI/2);
		choiseFloor3Rect   = new Rect(1580*scaleGUI/2, 495*scaleGUI/2, 390*scaleGUI/2, 95*scaleGUI/2);
		choiseFloor4Rect   = new Rect(1580*scaleGUI/2, 400*scaleGUI/2, 390*scaleGUI/2, 95*scaleGUI/2);
		choiseFloorFoneRect= new Rect(1580*scaleGUI/2, 210*scaleGUI/2, 390*scaleGUI/2, 387*scaleGUI/2);
		
		hideFurnitRect = new Rect(40*scaleGUI/2, 700*scaleGUI/2, 154*scaleGUI/2, 154*scaleGUI/2);
		choiseDoorRect = new Rect(1800*scaleGUI/2, 700*scaleGUI/2, 168*scaleGUI/2, 168*scaleGUI/2);
	}
	
	void OnGUI()
	{	
		GUI.skin = EmptySkin;//Choise Room
		if(GUI.Button(choiseRoomRect,scaleGUI==2? (choiseRoomBtn==true? choiseRoomTexClick : choiseRoomTex) : (choiseRoomBtn==true? choiseRoomTexClick2 :choiseRoomTex2)))
		{
			choiseRoomBtn = !choiseRoomBtn; 
		}	
		if(choiseRoomBtn)
		{
			GUI.skin = choiseRoomFoneSkin;
			GUI.Label(choiseRoomFoneRect, "");//Fone
			GUI.skin = scaleGUI==2? (choiseRoom1Btn!=true? choiseRoom1Skin : choiseRoomClick1Skin) : (choiseRoom1Btn!=true? choiseRoom1Skin2 : choiseRoomClick1Skin2);
			if(GUI.Button(choiseRoom1Rect, ""))
			{
				choiseRoom1Btn = true;
				choiseRoom2Btn = false;
				choiseRoom3Btn = false;
				choiseRoom4Btn = false;
			}
			
			GUI.skin = scaleGUI==2? (choiseRoom2Btn!=true? choiseRoom2Skin : choiseRoomClick2Skin) : (choiseRoom2Btn!=true? choiseRoom2Skin2 : choiseRoomClick2Skin2);
			if(GUI.Button(choiseRoom2Rect, ""))
			{
				choiseRoom1Btn = false;
				choiseRoom2Btn = true;
				choiseRoom3Btn = false;
				choiseRoom4Btn = false;
			}
			
			GUI.skin = scaleGUI==2? (choiseRoom3Btn!=true? choiseRoom3Skin : choiseRoomClick3Skin) : (choiseRoom3Btn!=true? choiseRoom3Skin2 : choiseRoomClick3Skin2);
			if(GUI.Button(choiseRoom3Rect, ""))
			{
				choiseRoom1Btn = false;
				choiseRoom2Btn = false;
				choiseRoom3Btn = true;
				choiseRoom4Btn = false;
			}
			
			GUI.skin = scaleGUI==2? (choiseRoom4Btn!=true? choiseRoom4Skin : choiseRoomClick4Skin) : (choiseRoom4Btn!=true? choiseRoom4Skin2 : choiseRoomClick4Skin2);
			if(GUI.Button(choiseRoom4Rect, ""))
			{
				choiseRoom1Btn = false;
				choiseRoom2Btn = false;
				choiseRoom3Btn = false;
				choiseRoom4Btn = true;
			}
		}
		
		/*GUI.skin = EmptySkin;//Choise Wall
		if(GUI.Button(choiseWallRect, scaleGUI==2? (choiseWallBtn==true? choiseWallTexClick : choiseWallTex) : (choiseWallBtn==true? choiseWallTexClick2 :choiseWallTex2)))
		{
			choiseWallBtn = !choiseWallBtn;
		}	
		if(choiseWallBtn)
		{
			if(choiseRoom4Btn)
			{
				GUI.skin = choiseWallFoneSkin;
				GUI.Label(choiseWallFoneRect, "");//Fone	
			}
			else
			{	
				GUI.skin = podkladka2CellSkin;
				GUI.Label(podkladka2CellRect, "");//Fone
			}
			GUI.skin = scaleGUI==2? choiseWall1Skin : choiseWall1Skin2;
			if(GUI.Button(choiseWall1Rect, ""))
			{
				choiseWall1Btn = true;
				choiseWall2Btn = false;
				choiseWall3Btn = false;
				SetWall();
			}
			
			GUI.skin = scaleGUI==2? choiseWall2Skin : choiseWall2Skin2;
			if(GUI.Button(choiseWall2Rect, ""))
			{
				choiseWall1Btn = false;
				choiseWall2Btn = true;
				choiseWall3Btn = false;
				SetWall();
			}
			
			if(choiseRoom4Btn)
			{
				GUI.skin = scaleGUI==2? choiseWall3Skin : choiseWall3Skin2;
				if(GUI.Button(choiseWall3Rect, ""))
				{
					choiseWall1Btn = false;
					choiseWall2Btn = false;
					choiseWall3Btn = true;
					SetWall();
				}
			}
		}
		*/
		GUI.skin = EmptySkin;//Choise Floor
		if(GUI.Button(choiseFloorRect, scaleGUI==2? (choiseFloorBtn==true? choiseFloorTexClick : choiseFloorTex) : (choiseFloorBtn==true? choiseFloorTexClick2 :choiseFloorTex2)))
		{
			choiseFloorBtn = !choiseFloorBtn;
		}	
		if(choiseFloorBtn)
		{
			if(choiseRoom2Btn)
			{
				GUI.skin = choiseFloorFoneSkin;
				GUI.Label(choiseFloorFoneRect, "");//Fone
				GUI.skin = scaleGUI==2? choiseFloor1Skin : choiseFloor1Skin2;
				if(GUI.Button(choiseFloor1Rect, ""))
				{
					choiseFloor1Btn = true;
					choiseFloor2Btn = false;
					choiseFloor3Btn = false;
					choiseFloor4Btn = false;
					SetFloor();
				}
				
				GUI.skin = scaleGUI==2? choiseFloor2Skin : choiseFloor2Skin2;
				if(GUI.Button(choiseFloor2Rect, ""))
				{
					choiseFloor1Btn = false;
					choiseFloor2Btn = true;
					choiseFloor3Btn = false;
					choiseFloor4Btn = false;
					SetFloor();
				}
				
				GUI.skin = scaleGUI==2? choiseFloor3Skin : choiseFloor3Skin2;
				if(GUI.Button(choiseFloor3Rect, ""))
				{
					choiseFloor1Btn = false;
					choiseFloor2Btn = false;
					choiseFloor3Btn = true;
					choiseFloor4Btn = false;
					SetFloor();
				}
				
				GUI.skin = scaleGUI==2? choiseFloor4Skin : choiseFloor4Skin2;
				if(GUI.Button(choiseFloor4Rect, ""))
				{
					choiseFloor1Btn = false;
					choiseFloor2Btn = false;
					choiseFloor3Btn = false;
					choiseFloor4Btn = true;
					SetFloor();
				}
			}
			else
			{
				GUI.skin = choiseWallFoneSkin;
				GUI.Label(new Rect(1580*scaleGUI/2, 210*scaleGUI/2, 390*scaleGUI/2, 291*scaleGUI/2), "");//Fone
				GUI.skin = scaleGUI==2? choiseFloor1Skin : choiseFloor1Skin2;
				if(GUI.Button(choiseFloor1Rect, ""))
				{
					choiseFloor1Btn = true;
					choiseFloor2Btn = false;
					choiseFloor3Btn = false;
					choiseFloor4Btn = false;
					SetFloor();
				}
				
				GUI.skin = scaleGUI==2? choiseFloor2Skin : choiseFloor2Skin2;
				if(GUI.Button(choiseFloor2Rect, ""))
				{
					choiseFloor1Btn = false;
					choiseFloor2Btn = true;
					choiseFloor3Btn = false;
					choiseFloor4Btn = false;
					SetFloor();
				}
				GUI.skin = scaleGUI==2? choiseFloor4Skin : choiseFloor4Skin2;
				if(GUI.Button(choiseFloor4Rect, ""))
				{
					choiseFloor1Btn = false;
					choiseFloor2Btn = false;
					choiseFloor3Btn = false;
					choiseFloor4Btn = true;
					SetFloor();
				}
			}		
		}
		/*
		GUI.skin = EmptySkin;//Choise Door
		if(GUI.Button(choiseDoorRect, scaleGUI==2? (choiseDoorBtn==true? choiseDoorTexClick : choiseDoorTex) : (choiseDoorBtn==true? choiseDoorTexClick2 :choiseDoorTex2))){choiseDoorBtn = !choiseDoorBtn;}	
		if(choiseDoorBtn)
		{
			
		}
		*/
		GUI.skin = scaleGUI==2? hideFurnitSkin : hideFurnitSkin2;
		if(GUI.Button(hideFurnitRect, ""))
		{
			HideFurniture();
		}
		
		GUI.skin = EmptySkin;//Choise Color
		if(GUI.Button(choiseColorRect, scaleGUI==2? choiseColorTex : choiseColorTex2)){choiseColorBtn = !choiseColorBtn;}	
		if(choiseColorBtn)
		{
			GUI.Label(choiseColorFoneRect, choiseColorFoneTex);
			if(GUI.Button(choiseColor1Rect, scaleGUI==2? choiseColor1Tex : choiseColor1Tex2)) 
			{
				choiseColor1Btn = !choiseColor1Btn;
				choiseColor2Btn = false;
				choiseColor3Btn = false;
			}
			if(choiseColor1Btn)
			{
				GUI.Label(choiseColorFloorFoneRect, choiseColorFloorFoneTex);
				if(GUI.Button(choiseColorFloor1Rect, scaleGUI==2? choiseColorFloor1Tex : choiseColorFloor1Tex2))
				{
					choiseTexLamBtn = !choiseTexLamBtn;
					choiseTexLinBtn = false;
					choiseTexKovBtn = false;
					choiseTexPliBtn = false;
				}
				if(GUI.Button(choiseColorFloor2Rect, scaleGUI==2? choiseColorFloor2Tex : choiseColorFloor2Tex2))
				{
					choiseTexLamBtn = false;
					choiseTexLinBtn = !choiseTexLinBtn;
					choiseTexKovBtn = false;
					choiseTexPliBtn = false;
				}
				if(GUI.Button(choiseColorFloor3Rect, scaleGUI==2? choiseColorFloor3Tex : choiseColorFloor3Tex2))
				{
					choiseTexLamBtn = false;
					choiseTexLinBtn = false;
					choiseTexKovBtn = !choiseTexKovBtn;
					choiseTexPliBtn = false;
				}
				if(GUI.Button(choiseColorFloor4Rect, scaleGUI==2? choiseColorFloor4Tex : choiseColorFloor4Tex2))
				{
					choiseTexLamBtn = false;
					choiseTexLinBtn = false;
					choiseTexKovBtn = false;
					choiseTexPliBtn = !choiseTexPliBtn;
				}
				if(choiseTexLamBtn)
				{
					GUI.Label(choiseTextureFoneTexRect, choiseTextureFoneTex);
					if(GUI.Button(firstTexture, TextureFloorLam1)){UpdateFloor(lam1Tex, new Vector2(0.2f,0.2f));}
					if(GUI.Button(secondTexture, TextureFloorLam2)){UpdateFloor(lam2Tex, new Vector2(0.2f,0.2f));}
				}
				if(choiseTexLinBtn)
				{
					GUI.Label(choiseTextureFoneTexRect, choiseTextureFoneTex);
					if(GUI.Button(firstTexture, TextureFloorLin1)){UpdateFloor(lin1Tex, new Vector2(0.2f,0.2f));}
					if(GUI.Button(secondTexture, TextureFloorLin2)){UpdateFloor(lin2Tex, new Vector2(0.2f,0.2f));}
				}
				if(choiseTexKovBtn)
				{
					GUI.Label(choiseTextureFoneTexRect, choiseTextureFoneTex);
					if(GUI.Button(firstTexture, TextureFloorKov1)){UpdateFloor(kov1Tex, new Vector2(0.2f,0.2f));}
					if(GUI.Button(secondTexture, TextureFloorKov2)){UpdateFloor(kov2Tex, new Vector2(0.2f,0.2f));}
				}
				if(choiseTexPliBtn)
				{
					GUI.Label(choiseTextureFoneTexRect, choiseTextureFoneTex);
					if(GUI.Button(firstTexture, TextureFloorPli1)){UpdateFloor(pli1Tex, new Vector2(4,4));}
					if(GUI.Button(secondTexture, TextureFloorPli2)){UpdateFloor(pli2Tex, new Vector2(4,4));}
				}
			}
			if(GUI.Button(choiseColor2Rect, scaleGUI==2? choiseColor2Tex : choiseColor2Tex2)) 
			{
				choiseColor2Btn = !choiseColor2Btn;
				choiseColor1Btn = false;
				choiseColor3Btn = false;
			}
			if(choiseColor2Btn)
			{
				GUI.Label(choiseColorWallFoneRect, choiseColorWallFoneTex);
				if(GUI.Button(choiseColorWall1Rect, scaleGUI==2? choiseColorWall1Tex : choiseColorWall1Tex2))
				{
					choiseTexPanBtn = !choiseTexPanBtn;
					choiseTexOboBtn = false;
					choiseTexWallPliBtn = false;
				}
				if(GUI.Button(choiseColorWall2Rect, scaleGUI==2? choiseColorWall2Tex : choiseColorWall2Tex2))
				{
					choiseTexPanBtn = false;
					choiseTexOboBtn = !choiseTexOboBtn;
					choiseTexWallPliBtn = false;
				}
				if(GUI.Button(choiseColorWall3Rect, scaleGUI==2? choiseColorWall3Tex : choiseColorWall3Tex2))
				{
					choiseTexPanBtn = false;
					choiseTexOboBtn = false;
					choiseTexWallPliBtn = !choiseTexWallPliBtn;
				}
				if(choiseTexPanBtn)
				{
					GUI.Label(choiseTextureFoneTexRect, choiseTextureFoneTex);
					if(GUI.Button(firstTexture, TextureWallPan1)){UpdateWall(pan1Tex);}
					if(GUI.Button(secondTexture, TextureWallPan2)){UpdateWall(pan2Tex);}
				}
				if(choiseTexOboBtn)
				{
					GUI.Label(choiseTextureFoneTexRect, choiseTextureFoneTex);
					if(GUI.Button(firstTexture, TextureWallObo1)){UpdateWall(obo1Tex);}
					if(GUI.Button(secondTexture, TextureWallObo2)){UpdateWall(obo2Tex);}
				}
				if(choiseTexWallPliBtn)	
				{
					GUI.Label(choiseTextureFoneTexRect, choiseTextureFoneTex);
					if(GUI.Button(firstTexture, TextureWallPli1)){UpdateWall(wallPli1Tex);}
					if(GUI.Button(secondTexture, TextureWallPli2)){UpdateWall(wallPli2Tex);}
				}
			}
			if(GUI.Button(choiseColor3Rect, scaleGUI==2? choiseColor3Tex : choiseColor3Tex2)) 
			{
				choiseColor3Btn = !choiseColor3Btn;
				choiseColor1Btn = false;
				choiseColor2Btn = false;
			}
			if(choiseColor3Btn)
			{
				GUI.Label(choiseColorDoorFoneRect, choiseColorDoorFoneTex);
				if(GUI.Button(choiseColorDoor1Rect, scaleGUI==2? choiseColorDoor1Tex : choiseColorDoor1Tex2))
				{
					choiseTexVhBtn = !choiseTexVhBtn;
					choiseTexVnBtn = false;
				}
				if(GUI.Button(choiseColorDoor2Rect, scaleGUI==2? choiseColorDoor2Tex : choiseColorDoor2Tex2))
				{
					choiseTexVhBtn = false;
					choiseTexVnBtn = !choiseTexVnBtn;
				}
				if(choiseTexVhBtn)
				{
					GUI.Label(choiseTextureFoneTexRect, choiseTextureDoorFoneTex);
					if(GUI.Button(TextureDoorVh1Rect, TextureDoorVh1))
					{
						GameObject.Find("dver vhodnaya2").transform.localScale = new Vector3(1,1,1);
						GameObject.Find("dver vhodnaya1").transform.localScale = new Vector3(0,0,0);
					}
					if(GUI.Button(TextureDoorVh2Rect, TextureDoorVh2))
					{
						GameObject.Find("dver vhodnaya1").transform.localScale = new Vector3(1,1,1);
						GameObject.Find("dver vhodnaya2").transform.localScale = new Vector3(0,0,0);
					}
				}
				if(choiseTexVnBtn)
				{
					GUI.Label(choiseTextureFoneTexRect, choiseTextureDoorFoneTex);
					if(GUI.Button(TextureDoorVh1Rect, TextureDoorVn1))
					{
						GameObject.Find("dver v vannu2").transform.localScale = new Vector3(1,1,1);
						GameObject.Find("dver v vannu1").transform.localScale = new Vector3(0,0,0);
					}
					if(GUI.Button(TextureDoorVh2Rect, TextureDoorVn2))
					{
						GameObject.Find("dver v vannu1").transform.localScale = new Vector3(1,1,1);
						GameObject.Find("dver v vannu2").transform.localScale = new Vector3(0,0,0);
					}
				}
			}
		}
	}
	
	void HideFurniture()
	{
		if(choiseRoom1Btn)
		{
			if(choiseHide1Btn) GameObject.Find("mebel koridor").transform.localScale = new Vector3(1,1,1);
			else GameObject.Find("mebel koridor").transform.localScale = new Vector3(0,0,0);
			choiseHide1Btn=!choiseHide1Btn;
		}
		else if(choiseRoom2Btn)
		{
			if(choiseHide2Btn) GameObject.Find("mebel komnata").transform.localScale = new Vector3(1,1,1);
			else GameObject.Find("mebel komnata").transform.localScale = new Vector3(0,0,0);
			choiseHide2Btn=!choiseHide2Btn;
		}
		else if(choiseRoom3Btn)
		{
			if(choiseHide3Btn) GameObject.Find("mebel kuhnya").transform.localScale = new Vector3(1,1,1);
			else GameObject.Find("mebel kuhnya").transform.localScale = new Vector3(0,0,0);
			choiseHide3Btn=!choiseHide3Btn;
		}
		else if(choiseRoom4Btn)
		{
			if(choiseHide4Btn) GameObject.Find("mebel vanna").transform.localScale = new Vector3(1,1,1);
			else GameObject.Find("mebel vanna").transform.localScale = new Vector3(0,0,0);
			choiseHide4Btn=!choiseHide4Btn;
		}
	}
	
	void UpdateFloor(Texture Tex, Vector2 texScale)
	{
		if(choiseRoom1Btn)
		{
			SetUnitScale("koridor pol beton"); SetZeroScale("koridor pol linoleum","koridor pol plitka","koridor pol laminat");
			GameObject.Find("koridor pol beton").renderer.material.mainTexture = Tex;
			GameObject.Find("koridor pol beton").renderer.material.mainTextureScale = texScale;
		}
		else if(choiseRoom2Btn)
		{
			SetUnitScale("komnata pol beton"); SetZeroScale("komnata pol linoleum","komnata pol plitka", "komnata pol kover","komnata pol laminat");
			GameObject.Find("komnata pol beton").renderer.material.mainTexture = Tex;
			GameObject.Find("komnata pol beton").renderer.material.mainTextureScale = texScale;
		}
		else if(choiseRoom3Btn)
		{
			SetUnitScale("kuhnya pol beton"); SetZeroScale("kuhnya pol linoleum","kuhnya pol plitka","kuhnya pol laminat");
			GameObject.Find("kuhnya pol beton").renderer.material.mainTexture = Tex;
			GameObject.Find("kuhnya pol beton").renderer.material.mainTextureScale = texScale;
		}
		else if(choiseRoom4Btn)
		{
			SetUnitScale("vanna pol beton"); SetZeroScale("vanna pol linoleum","vanna pol plitka","vanna pol laminat");
			GameObject.Find("vanna pol beton").renderer.material.mainTexture = Tex;
			GameObject.Find("vanna pol beton").renderer.material.mainTextureScale = texScale;
		}
	}
	
	void UpdateWall(Texture Tex)
	{
		if(choiseRoom1Btn)
		{
			GameObject.Find("koridor stena paneli").transform.localScale = new Vector3(0,0,0);
			GameObject.Find("koridor stena oboi").transform.localScale = new Vector3(0,0,0);
			GameObject.Find("koridor stena beton").transform.localScale = new Vector3(1,1,1);
			GameObject.Find("koridor stena beton").renderer.material.mainTexture = Tex;
		}
		else if(choiseRoom2Btn)
		{
			GameObject.Find("komnata stena paneli").transform.localScale = new Vector3(0,0,0);
			GameObject.Find("komnata stena oboi").transform.localScale = new Vector3(0,0,0);
			GameObject.Find("komnata stena beton").transform.localScale = new Vector3(1,1,1);
			GameObject.Find("komnata stena beton").renderer.material.mainTexture = Tex;
			if(Tex == obo2Tex)GameObject.Find("komnata stena beton").renderer.material.mainTexture = obo3Tex;
		}
		else if(choiseRoom3Btn)
		{
			GameObject.Find("kuhnya steny paneli").transform.localScale = new Vector3(0,0,0);
			GameObject.Find("kuhnya steny oboi").transform.localScale = new Vector3(0,0,0);
			GameObject.Find("kuhnya steny beton").transform.localScale = new Vector3(1,1,1);
			GameObject.Find("kuhnya steny beton").renderer.material.mainTexture = Tex;
		}
		else if(choiseRoom4Btn)
		{
			GameObject.Find("stena vanna panely").transform.localScale = new Vector3(0,0,0);
			GameObject.Find("stena vanna plitka").transform.localScale = new Vector3(0,0,0);
			GameObject.Find("stena vanna beton").transform.localScale = new Vector3(1,1,1);
			GameObject.Find("stena vanna oboi").transform.localScale = new Vector3(0,0,0);
			GameObject.Find("stena vanna beton").renderer.material.mainTexture = Tex;
		}
	}
	
	void SetWall()
	{
		if(choiseRoom1Btn)
		{
			if(choiseWall1Btn) {GameObject.Find("koridor stena paneli").transform.localScale = new Vector3(1,1,1);
								GameObject.Find("koridor stena oboi").transform.localScale = new Vector3(0,0,0);
								GameObject.Find("koridor stena beton").transform.localScale = new Vector3(0,0,0);}
			if(choiseWall2Btn) {GameObject.Find("koridor stena oboi").transform.localScale = new Vector3(1,1,1);
								GameObject.Find("koridor stena paneli").transform.localScale = new Vector3(0,0,0);
								GameObject.Find("koridor stena beton").transform.localScale = new Vector3(0,0,0);}
		}
		else if(choiseRoom2Btn)
		{
			if(choiseWall1Btn) {GameObject.Find("komnata stena paneli").transform.localScale = new Vector3(1,1,1);
								GameObject.Find("komnata stena oboi").transform.localScale = new Vector3(0,0,0);
								GameObject.Find("komnata stena beton").transform.localScale = new Vector3(0,0,0);}
			if(choiseWall2Btn) {GameObject.Find("komnata stena oboi").transform.localScale = new Vector3(1,1,1);
								GameObject.Find("komnata stena paneli").transform.localScale = new Vector3(0,0,0);
								GameObject.Find("komnata stena beton").transform.localScale = new Vector3(0,0,0);}
		}
		else if(choiseRoom3Btn)
		{
			if(choiseWall1Btn) {GameObject.Find("kuhnya steny paneli").transform.localScale = new Vector3(1,1,1);
								GameObject.Find("kuhnya steny oboi").transform.localScale = new Vector3(0,0,0);
								GameObject.Find("kuhnya steny beton").transform.localScale = new Vector3(0,0,0);}
			if(choiseWall2Btn) {GameObject.Find("kuhnya steny oboi").transform.localScale = new Vector3(1,1,1);
								GameObject.Find("kuhnya steny paneli").transform.localScale = new Vector3(0,0,0);
								GameObject.Find("kuhnya steny beton").transform.localScale = new Vector3(0,0,0);}
		}
		else if(choiseRoom4Btn)
		{
			if(choiseWall2Btn) {GameObject.Find("stena vanna panely").transform.localScale = new Vector3(1,1,1);
								GameObject.Find("stena vanna plitka").transform.localScale = new Vector3(0,0,0);
								GameObject.Find("stena vanna beton").transform.localScale = new Vector3(0,0,0);
								GameObject.Find("stena vanna oboi").transform.localScale = new Vector3(0,0,0);}
			if(choiseWall3Btn) {GameObject.Find("stena vanna oboi").transform.localScale = new Vector3(1,1,1);
								GameObject.Find("stena vanna panely").transform.localScale = new Vector3(0,0,0);
								GameObject.Find("stena vanna beton").transform.localScale = new Vector3(0,0,0);
								GameObject.Find("stena vanna plitka").transform.localScale = new Vector3(0,0,0);}
			if(choiseWall3Btn) {GameObject.Find("stena vanna plitka").transform.localScale = new Vector3(1,1,1);
								GameObject.Find("stena vanna panely").transform.localScale = new Vector3(0,0,0);
								GameObject.Find("stena vanna oboi").transform.localScale = new Vector3(0,0,0);
								GameObject.Find("stena vanna beton").transform.localScale = new Vector3(0,0,0);}
				
		}
	}
	
	void SetFloor()
	{
		if(choiseRoom1Btn)
		{
			if(choiseFloor1Btn){SetUnitScale("koridor pol laminat"); SetZeroScale("koridor pol linoleum","koridor pol plitka","koridor pol beton");}
			if(choiseFloor2Btn){SetUnitScale("koridor pol linoleum"); SetZeroScale("koridor pol laminat","koridor pol plitka","koridor pol beton");}
			if(choiseFloor4Btn){SetUnitScale("koridor pol plitka"); SetZeroScale("koridor pol laminat","koridor pol linoleum","koridor pol beton");}
		}
		else if(choiseRoom2Btn)
		{
			if(choiseFloor1Btn){SetUnitScale("komnata pol laminat"); SetZeroScale("komnata pol linoleum","komnata pol plitka", "komnata pol kover","komnata pol beton");}
			if(choiseFloor2Btn){SetUnitScale("komnata pol linoleum"); SetZeroScale("komnata pol laminat","komnata pol plitka", "komnata pol kover","komnata pol beton");}
			if(choiseFloor3Btn){SetUnitScale("komnata pol kover","komnata pol beton"); SetZeroScale("komnata pol laminat","komnata pol plitka", "komnata pol linoleum");}
			if(choiseFloor4Btn){SetUnitScale("komnata pol plitka"); SetZeroScale("komnata pol laminat","komnata pol linoleum", "komnata pol kover","komnata pol beton");}
		}
		else if(choiseRoom3Btn)
		{
			if(choiseFloor1Btn){SetUnitScale("kuhnya pol laminat"); SetZeroScale("kuhnya pol linoleum","kuhnya pol plitka","kuhnya pol beton");}
			if(choiseFloor2Btn){SetUnitScale("kuhnya pol linoleum"); SetZeroScale("kuhnya pol laminat","kuhnya pol plitka","kuhnya pol beton");}
			if(choiseFloor4Btn){SetUnitScale("kuhnya pol plitka"); SetZeroScale("kuhnya pol laminat","kuhnya pol linoleum","kuhnya pol beton");}
		}
		else if(choiseRoom4Btn)
		{
			if(choiseFloor1Btn){SetUnitScale("vanna pol laminat"); SetZeroScale("vanna pol linoleum","vanna pol plitka","vanna pol beton");}
			if(choiseFloor2Btn){SetUnitScale("vanna pol linoleum"); SetZeroScale("vanna pol laminat","vanna pol plitka","vanna pol beton");}
			if(choiseFloor4Btn){SetUnitScale("vanna pol plitka"); SetZeroScale("vanna pol laminat","vanna pol linoleum","vanna pol beton");}
		}
	}
		
	
	void SetUnitScale(params string[] list)
	{
		foreach(string val in list)
		{
			GameObject.Find(val).transform.localScale = new Vector3(1,1,1);
		}
	}
	void SetZeroScale(params string[] list)
	{
		foreach(string val in list)
		{
			GameObject.Find(val).transform.localScale = new Vector3(0,0,0);
		}
	}
	void SetGOTexture(GameObject[] GO, Texture tex)
	{
		foreach(GameObject tmp in GO)
		{
			tmp.renderer.material.mainTexture = tex;
		}
	}
	void SetGUITexture(Texture2D normal, Texture2D Clicked, GUISkin skin)
	{
		skin.button.normal.background   = normal;
		skin.button.active.background   = Clicked;
		skin.button.hover.background    = Clicked;
		skin.button.focused.background  = Clicked;
		skin.button.onNormal.background = Clicked;
		skin.button.onActive.background = Clicked;
		skin.button.onHover.background  = Clicked;
		skin.button.onFocused.background= Clicked;
	}
	
	void Swap<T>(ref T lhs, ref T rhs)
	{
	    T temp;
	    temp = lhs;
	    lhs = rhs;
	    rhs = temp;
	}
}
