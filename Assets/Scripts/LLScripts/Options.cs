/*
 * скрипт ленивой загрузки опций 
 */
using UnityEngine;
using System.Collections;

public class Options : MonoBehaviour 
{
	Rect siteRect;
	Rect wingsMiniRect;
	Rect optionTextRect;
	
	public GUISkin siteSkin;
	public GUISkin wingsMiniSkin;
	public GUISkin optionTextSkin;
	public GUISkin toogleSoundSkin;
	public GUISkin toogleCtrlSkin;
	public GUISkin siteSkin2;
	public GUISkin wingsMiniSkin2;
	public GUISkin optionTextSkin2;
	public GUISkin restorePurSkin2;
	public GUISkin toogleSoundSkin2;
	public GUISkin toogleCtrlSkin2;
	
	int scaleGUI;
	MainMenu other;
	void Start () 
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		scaleGUI = Screen.width/1024;
		optionTextRect	 = new Rect(0*scaleGUI, 130*scaleGUI, 1024*scaleGUI, 53*scaleGUI);
		
		wingsMiniRect = new Rect(480*scaleGUI, 50*scaleGUI, 61.5f*scaleGUI, 69.5f*scaleGUI);
		siteRect      = new Rect(458*scaleGUI, 700*scaleGUI, 95*scaleGUI, 37*scaleGUI);
	}
	
	void OnGUI () 
	{
		GUI.skin= scaleGUI==2? siteSkin : siteSkin2;
    	if(GUI.Button(siteRect,""))//Site 
		{
    	    Application.OpenURL("http://www.ardriftracing.com");
    	}
		GUI.skin= scaleGUI==2? wingsMiniSkin : wingsMiniSkin2;
    	if(GUI.Button(wingsMiniRect,"")){}//WingsMini
			
		GUI.skin= scaleGUI==2? toogleSoundSkin : toogleSoundSkin2;
    	other.toggleTxt = GUI.Toggle(new Rect(550*scaleGUI, 340*scaleGUI, 150*scaleGUI, 53*scaleGUI), other.toggleTxt, "");
		
		GUI.skin= scaleGUI==2? toogleCtrlSkin : toogleCtrlSkin2;
		other.inverse = GUI.Toggle(new Rect(350*scaleGUI, 340*scaleGUI, 161*scaleGUI, 53*scaleGUI), other.inverse, "");
		
		GUI.skin= scaleGUI==2? optionTextSkin : optionTextSkin2;
		if(GUI.Button(optionTextRect,"")){}
	}
}
