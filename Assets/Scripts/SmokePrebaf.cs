/*
 *  выводит время гонки и отвечает за звук и дым, только при старте
 */ 
using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class SmokePrebaf : MonoBehaviour 
{
	public WheelCollider CorrespondingCollider;
	public GameObject SlipPrefab;
	float RotationValue = 0.0f;
	float timeStart;
	float timeMoveOFF;
	public AudioClip AStart;
	AudioSource start;
	MainMenu other;
	bool firstMove;
	bool go;
	float timeInGame;
	float timeToSave;
	string path;
	public Joystick moveTouchPad;
	public Joystick touchPadBack;
	bool goTime;
	int scaleGUI;
	public GUISkin textStyle; 
	public GUISkin textStyle2;
	// Use this for initialization
	void Start () 
	{
		scaleGUI =Screen.width/1024;
		goTime=false;
		path = GetPath() + "/TimeInGame.txt";
		timeToSave=0.0f;
		timeInGame=0.0f;
		go=false;
		firstMove=false;
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		timeStart = 0.0f;
		timeMoveOFF = 0.0f;
		start=gameObject.AddComponent<AudioSource>();
		start.loop = false;
		start.playOnAwake = false;
		start.clip = AStart;
		start.panLevel = 0;
	}
	
	void StartMove()
	{
		if(start.isPlaying) return;
		start.Play();
	}
	void Update () 
	{
		if(other.toggleTxt)
		{
			start.volume = 0;
		}
		else 
		{
			start.volume = 1;
		}
		if(this.gameObject.name=="wheel02 Back R")
		{
			if(moveTouchPad.position.y > 0.01 || touchPadBack.position.y > 0.01)
			{
				goTime=true;
			}
			if(goTime && !other.pauseOnGameMode)
			{
				timeInGame+=Time.deltaTime;
				timeToSave+=Time.deltaTime;
				SaveScore(System.Convert.ToString(timeInGame));
			}
		}
		RaycastHit hit;
		Vector3 ColliderCenterPoint = CorrespondingCollider.transform.TransformPoint( CorrespondingCollider.center );
		
		if ( Physics.Raycast( ColliderCenterPoint, -CorrespondingCollider.transform.up, out hit, CorrespondingCollider.suspensionDistance + CorrespondingCollider.radius ) ) 
		{
			transform.position = hit.point + (CorrespondingCollider.transform.up * CorrespondingCollider.radius);
		}
		else
		{
			transform.position = ColliderCenterPoint - (CorrespondingCollider.transform.up * CorrespondingCollider.suspensionDistance);
		}
		
		transform.rotation = CorrespondingCollider.transform.rotation * Quaternion.Euler( RotationValue, CorrespondingCollider.steerAngle, 0 );
		RotationValue += CorrespondingCollider.rpm * ( 360/60 ) * Time.deltaTime;
		WheelHit CorrespondingGroundHit;
		CorrespondingCollider.GetGroundHit(out CorrespondingGroundHit);
		if(moveTouchPad.position.y > 0)
		{
			if (SlipPrefab )
			{
				timeStart+=Time.deltaTime;
				if(timeStart<1)
				{
					if(timeMoveOFF>2 || !firstMove)
					{
						firstMove=true;
						timeMoveOFF=0.0f;
						go=true;
						StartMove();
					}
					if(go)
					{
						Instantiate( SlipPrefab, CorrespondingGroundHit.point, Quaternion.identity );
					}

				}
				else
				{
					go=false;	
				}
			}
		}
		else
		{
			timeStart=0.0f;
			timeMoveOFF+=Time.deltaTime;
		}
	}
	void SaveScore(string time) 
	{ 
		if(timeToSave>1)
		{ 
			timeToSave=0.0f;
			using (StreamWriter sw = File.CreateText(path))
       		{
       		    sw.WriteLine(time);
				sw.Close();
       		}
	    	
	    }
	}
	string GetPath()
	{
		string root = Application.dataPath.Substring(0, Application.dataPath.Length - 13) + "/Documents";
		#if UNITY_EDITOR
		root = Application.dataPath.Substring(0, Application.dataPath.Length);
		#endif
        return root;
	}
	void OnGUI()
	{
		if(this.gameObject.name=="wheel02 Back R")
		{
			GUI.skin = scaleGUI==2? textStyle : textStyle2;
			string stringToEdit=System.Convert.ToString(Math.Round(timeInGame));
			GUI.Button (new Rect (900*scaleGUI,50*scaleGUI,100*scaleGUI*2,30*scaleGUI*2), "Time: "+stringToEdit);   //25
		}
	}
		
}
