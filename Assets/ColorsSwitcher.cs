using UnityEngine;
using System.Collections;

public class ColorsSwitcher : MonoBehaviour 
{
	public Texture RedButton;
	public Texture GrayButton;
	public Texture BlackButton;
	
	public Texture RedTex;
	public Texture GrayTex;
	public Texture BlackTex;
	
	public GUISkin empty;

	void OnGUI()
	{
		GUI.skin = empty;
		if(GUI.Button(new Rect(10/2,650/2,153/2,153/2), RedButton))
		{
			GameObject.Find("part_012").renderer.material.mainTexture = RedTex;
			GameObject.Find("part_015").renderer.material.mainTexture = RedTex;
			GameObject.Find("part_013").renderer.material.mainTexture = RedTex;
			GameObject.Find("part_000").renderer.material.mainTexture = RedTex;
			GameObject.Find("part_001").renderer.material.mainTexture = RedTex;
			GameObject.Find("part_002").renderer.material.mainTexture = RedTex;
			GameObject.Find("part_014").renderer.material.mainTexture = RedTex;
			GameObject.Find("part_031").renderer.material.mainTexture = RedTex;
			GameObject.Find("part_033").renderer.material.mainTexture = RedTex;
		}
		if(GUI.Button(new Rect(10/2,810/2,153/2,153/2), GrayButton))
		{
			GameObject.Find("part_012").renderer.material.mainTexture = GrayTex;
			GameObject.Find("part_015").renderer.material.mainTexture = GrayTex;
			GameObject.Find("part_013").renderer.material.mainTexture = GrayTex;
			GameObject.Find("part_000").renderer.material.mainTexture = GrayTex;
			GameObject.Find("part_001").renderer.material.mainTexture = GrayTex;
			GameObject.Find("part_002").renderer.material.mainTexture = GrayTex;
			GameObject.Find("part_014").renderer.material.mainTexture = GrayTex;
			GameObject.Find("part_031").renderer.material.mainTexture = GrayTex;
			GameObject.Find("part_033").renderer.material.mainTexture = GrayTex;
		}
		if(GUI.Button(new Rect(10/2,970/2,153/2,153/2), BlackButton))
		{
			GameObject.Find("part_012").renderer.material.mainTexture = BlackTex;
			GameObject.Find("part_015").renderer.material.mainTexture = BlackTex;
			GameObject.Find("part_013").renderer.material.mainTexture = BlackTex;
			GameObject.Find("part_000").renderer.material.mainTexture = BlackTex;
			GameObject.Find("part_001").renderer.material.mainTexture = BlackTex;
			GameObject.Find("part_002").renderer.material.mainTexture = BlackTex;
			GameObject.Find("part_014").renderer.material.mainTexture = BlackTex;
			GameObject.Find("part_031").renderer.material.mainTexture = BlackTex;
			GameObject.Find("part_033").renderer.material.mainTexture = BlackTex;
		}
	}
}
