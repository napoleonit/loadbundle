using UnityEngine;
using System.Collections;

public class dot3 : MonoBehaviour 
{
	FinishSecondScene otherFin;
	// Use this for initialization
	void Start () 
	{
		otherFin = GameObject.Find("Finish").GetComponent("FinishSecondScene") as FinishSecondScene;
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.name=="chassis22" && otherFin.dot[1])
		{
			otherFin.dot[2]=true;
		}
    }
}
