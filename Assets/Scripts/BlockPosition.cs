using UnityEngine;
using System.Collections;

public class BlockPosition : MonoBehaviour {

	float startPos;
	bool stay;
	bool go;
	int i=0;
	GuiBlock other;
	public int count;
	void Start () 
	{
		stay=false;
		go=true;
		startPos=this.transform.position.y;
		other = GameObject.Find("Gui").GetComponent("GuiBlock") as GuiBlock;
	}
	
	void Update () 
	{
		if(count==2)return;
		if(Input.GetMouseButtonDown(0) && go)
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        	   RaycastHit hit; 
        	   if (Physics.Raycast(ray, out hit, Mathf.Infinity)) 
        	   {    
        	       //	GameObject.Find("roadblock low"+other.i).transform.position = new Vector3(hit.point.x, startPos, hit.point.z);
					count++;
        	   }
		}
	}
}
