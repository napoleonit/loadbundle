/*
 * что-то связанное с покупкой
 */ 
using UnityEngine;
using System.Collections;

public class Product : MonoBehaviour
{
	public static MainMenu other;
	void Start()
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
	}
	public static void ProductPurchased(string productID)
	{
		other.isPurchased=true;
	}
	public static void ProductCancelled(string productID)
	{
		other.isPurchased=false;
	}
	public static void ProductRestored(string productID)
	{
		other.isPurchased=true;
	}
	public static void ProductFailed(string productID)
	{
		other.isPurchased=false;
	}
}
