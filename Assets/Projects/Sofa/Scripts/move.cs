using UnityEngine;
using System.Collections;

public class move : MonoBehaviour 
{
	Vector3 tapLast;
	// Use this for initialization
	void Start () 
	{
		this.transform.localEulerAngles = new Vector3(-90,180,0);
	}
	
	// Update is called once per frame
	void Update () 
	{
		foreach(var touch in Input.touches)
		{
			if (touch.phase == TouchPhase.Moved && Input.touchCount==1)
			{   
				tapLast = new Vector3 (this.transform.position.x + touch.deltaPosition.x/10, this.transform.position.y, this.transform.position.z+touch.deltaPosition.y/10);
        		this.transform.position = tapLast;
			}
		}
	}
}

