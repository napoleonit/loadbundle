/*
 * устанавливает ткстуры у джойстиков
 */ 
using UnityEngine;
using System.Collections;

public class SetJoyTexture : MonoBehaviour 
{
	MainMenu other;
	public Joystick touchPad;
	public Texture2D normalTexture;
	public Texture2D clickTexture;
	public Texture2D normalTexture2;
	public Texture2D clickTexture2;
	int scaleGUI;
	void Start () 
	{
		scaleGUI = Screen.width/1024;
	}
	
	void Update() 
	{
		if(touchPad.position.y>0)
		{
			this.guiTexture.texture = scaleGUI==2? clickTexture : clickTexture2;
		}
		else this.guiTexture.texture = scaleGUI==2? normalTexture : normalTexture2;
	}
}
