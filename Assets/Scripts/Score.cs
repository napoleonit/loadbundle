/*
 * выводит очки и время прошедшего уровня в сцене Score
 */

using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class Score : MonoBehaviour 
{
	MainMenu menu;
	float time;
	string path;
	string pathMinus;
	int score;
	float timeInGame;
	int scoreMinus;
	int count=0;
	string pathTime;
	string thisLvl;
	// Use this for initialization
	void Start () 
	{
		pathTime = GetPath() + "/TimeInGame.txt";
		pathMinus=GetPath()+"/ScoreMinus.txt";
		path=GetPath()+"/Score.txt";
		menu = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		thisLvl=menu.GetThisLvlDir();
		time=0;
		
		using (StreamReader sr = File.OpenText(path)) 
        {
            string s = "";
			s = sr.ReadLine();
			score=int.Parse(s);
			sr.Close();
        }
		using (StreamReader sr = File.OpenText(pathTime)) 
        {
            string s = "";
			s = sr.ReadLine();
			timeInGame=float.Parse(s);
			sr.Close();
        }
		using (StreamReader sr = File.OpenText(pathMinus)) 
        {
            string s = "";
			s = sr.ReadLine();
			scoreMinus=int.Parse(s);
			sr.Close();
        }
		count=score-scoreMinus;
		menu.timeInGame = float.Parse(System.Convert.ToString(Math.Round(timeInGame, 2)));
		menu.scoreInGame = count;
		SaveDriftScoreAndTime();
	}
	
	void Update () 
	{
		this.guiText.text="Your Time: "+Math.Round(timeInGame, 2)+"    Score: "+System.Convert.ToString(count);
	}
	
	string GetPath()
	{
		string root = Application.dataPath.Substring(0, Application.dataPath.Length - 13) + "/Documents";
		#if UNITY_EDITOR
		root = Application.dataPath.Substring(0, Application.dataPath.Length);
		#endif
        return root;
	}
	void SaveDriftScoreAndTime()
	{
       	using (StreamWriter sw = File.CreateText(thisLvl))
       	{
       	    sw.WriteLine(count);
			sw.WriteLine(Math.Round(timeInGame, 2));
			sw.Close();
       	}
	}
}
