/*
 * скрипт загрузки бандла хтп
 */
using UnityEngine;
using System.Collections;

public class HowToPlayLoader : MonoBehaviour {

	public GameObject go;
	public static AssetBundle bundle;
	static int count=1;
	int scaleGUI;
	IEnumerator Start () 
	{
		scaleGUI = Screen.width/1024;
		string url ="file://"+Application.dataPath.Substring(0, Application.dataPath.Length - 13) + "/car.app/QCAR/ChoiseTrack.unity3d";
    	WWW www = WWW.LoadFromCacheOrDownload (url, count++);
    	yield return www;
    	bundle = www.assetBundle;
    	go = bundle.Load("HowToPlay", typeof(GameObject)) as GameObject;
    	Instantiate(go);
	}
}
