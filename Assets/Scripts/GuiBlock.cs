using UnityEngine;
using System.Collections;

public class GuiBlock : MonoBehaviour 
{
	public GameObject q;
	float speed;
	string str;
	void OnGUI() 
	{
		speed=Vector3.Dot (q.rigidbody.velocity, q.transform.forward);
		str=System.Convert.ToString(speed);
        GUI.TextField(new Rect(10, 100, 100, 30), str, 25);
    }
}