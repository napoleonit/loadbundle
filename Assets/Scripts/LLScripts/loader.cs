/*
 * скрипт подгрузки бандла выбора трасс
 */
using UnityEngine;
using System.Collections;

public class loader : MonoBehaviour 
{
	public GameObject go;
	public static AssetBundle bundle;
	static int count=1;
	int scaleGUI;
	IEnumerator Start () 
	{
		scaleGUI = Screen.width/1024;
		string url ="file://"+Application.dataPath.Substring(0, Application.dataPath.Length - 13) + "/car.app/QCAR/ChoiseTrack.unity3d";
    	WWW www = WWW.LoadFromCacheOrDownload (url, count++);
    	yield return www;
    	bundle = www.assetBundle;
    	go = scaleGUI==2? bundle.Load("ChoiseTrack3", typeof(GameObject)) as GameObject : bundle.Load("ChoiseTrack2", typeof(GameObject)) as GameObject;
    	Instantiate(go);
	}
}
